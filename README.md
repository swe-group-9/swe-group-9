# Catching Congress

Group Number: 11am Group 9

|   Group Member  |   EID   |    GitLab ID    |
| --------------- | ------- | --------------  |
| An Vi Nguyen    | an27366 | @an-vi          |
| Carson Saldanha | ccs3424 | @carsonsaldanha |
| Daksh Dua       | dd34362 | @dakshdua       |
| Nathan Gates    | nag2486 | @nathangates117 |
| Nisha Ramesh    | nr23799 | @nisharam7      |


Website: https://www.catchingcongress.me

GitLab Pipelines: https://gitlab.com/catchingcongress/catchingcongress/-/pipelines

Presentation Video: https://youtu.be/C8zHL9MCVz4

## Phase 1
* Git SHA: 574f96506d5bee2228b5e8c120c24ab2cf5e5d00
* Project Leader: Daksh Dua
* Comments: N/A

|   Group Member  | Estimated Time | Actual Time |
| --------------- | -------------- | ----------- |
| An Vi Nguyen    | 10             | 3           |
| Carson Saldanha | 10             | 13          |
| Daksh Dua       | 15             | 20          |
| Nathan Gates    | 20             | 11          |
| Nisha Ramesh    | 15             | 7           |

## Phase 2
* Git SHA: 82e4e156ac959b739b3eb5a396e7588caf85e0fb
* Project Leader: Carson Saldanha
* Comments: Some parts of the Home and About pages were inspired using the same MUI components as [SafeTravels](https://gitlab.com/dferndz/cs-373-project/). Parts of the GitLab repository scraping and backend model and schema structure were also sourced from [Texas Votes](https://gitlab.com/forbesye/fitsbits/). Parts of the GUI test script of the GitLab CI were sourced from [Cultured Foods](https://gitlab.com/cs373-group-11/cultured-foodies). The custom pagination was also inspired by [MusicCity](https://gitlab.com/m3263/musiccity).

|   Group Member  | Estimated Time | Actual Time |
| --------------- | -------------- | ----------- |
| An Vi Nguyen    | 40             | 22          |
| Carson Saldanha | 50             | 72          |
| Daksh Dua       | 50             | 72          |
| Nathan Gates    | 50             | 20          |
| Nisha Ramesh    | 50             | 19          |

## Phase 3
* Git SHA: dde0b85e09d5fd2e525e62ef01c1192311a62d91
* Project Leader: Nathan Gates
* Comments: Parts of the backend filtering, sorting, and searching were inspired by [Texas Votes](https://gitlab.com/forbesye/fitsbits/). The structure of some of the GUI tests were inspired by [WeLikeSportz](https://gitlab.com/debbiew27/WeLikeSportz/).

|   Group Member  | Estimated Time | Actual Time |
| --------------- | -------------- | ----------- |
| An Vi Nguyen    | 15             | 9           |
| Carson Saldanha | 35             | 28          |
| Daksh Dua       | 20             | 10          |
| Nathan Gates    | 20             | 8           |
| Nisha Ramesh    | 15             | 10          |

## Phase 4
* Git SHA: b483abaad3e3ff04048f0bec2b73735caf0f0516
* Project Leader: Carson Saldanha
* Comments: N/A

|   Group Member  | Estimated Time | Actual Time |
| --------------- | -------------- | ----------- |
| An Vi Nguyen    | 7              | 5           |
| Carson Saldanha | 20             | 11          |
| Daksh Dua       | 10             | 15          |
| Nathan Gates    | 8              | 4           |
| Nisha Ramesh    | 10             | 5           |
