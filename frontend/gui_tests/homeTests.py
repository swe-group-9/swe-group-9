import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service

URL = "https://www.catchingcongress.me/"

class HomeTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testSiteMotto(self):
        element = self.driver.find_element(By.ID, "motto")
        self.assertEqual(element.text, "Keeping Congress in check.")

    def testSiteTagline(self):
        element = self.driver.find_element(By.ID, "tagline")
        self.assertEqual(element.text, "Lobbying data on your politicians.")

    def testCongresspeopleCard(self):
        self.driver.find_element(By.ID, "congresspeopleCard").click()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "congresspeople")
        self.driver.back()

    def testCommitteesCard(self):
        self.driver.find_element(By.ID, "committeesCard").click()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "committees")
        self.driver.back()

    def testIndustriesCard(self):
        self.driver.find_element(By.ID, "industriesCard").click()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "industries")
  
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
