import unittest
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.catchingcongress.me/committees"

class CommitteesTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testCommitteesTitle(self):
        element = self.driver.find_element(By.ID, "committeesPageTitle")
        self.assertEqual(element.text, "Committees")
    
    def testCommitteesSearch(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "searchBar"))
            )
        except Exception as ex:
            print("Couldn't find committees search bar: " + str(ex))
    
    def testCommitteesFilter(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "chamberFilter"))
            )
        except Exception as ex:
            print("Couldn't find committees chamber filter: " + str(ex))
    
    def testCommitteesSort(self):
        try:
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, "sortMenu"))
            )
        except Exception as ex:
            print("Couldn't find committees sort menu: " + str(ex))
  
if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
