import os
from sys import platform

if __name__ == "__main__":
    if platform.startswith("win"):
        PATH = "./gui_tests/chromedriver.exe"
    elif platform.startswith("linux"):
        PATH = "./gui_tests/chromedriver"
    else:
        print("Unsupported OS")
        exit(-1)

    os.system("python ./gui_tests/homeTests.py " + PATH)
    os.system("python ./gui_tests/congresspeopleTests.py " + PATH)
    os.system("python ./gui_tests/committeesTests.py " + PATH)
    os.system("python ./gui_tests/industriesTests.py " + PATH)
    os.system("python ./gui_tests/aboutTests.py " + PATH)
