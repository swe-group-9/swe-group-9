import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Congresspeople from '../pages/congresspeople/CongresspeoplePage';

describe('Congresspeople Page Testing', () => {
  test('Renders without crashing', () => {
    render(
      <MemoryRouter>
        <Congresspeople />
      </MemoryRouter>
    );
    const element = screen.getAllByText('Congresspeople')[0];
    expect(element).toBeInTheDocument();
  });

  test('Renders searching', () => {
    render(
      <MemoryRouter>
        <Congresspeople />
      </MemoryRouter>
    );
    const element = screen.getByTestId('search');
    expect(element).toBeInTheDocument();
  });

  test('Renders filtering', () => {
    render(
      <MemoryRouter>
        <Congresspeople />
      </MemoryRouter>
    );
    const element = screen.getByTestId('filter');
    expect(element).toBeInTheDocument();
  });

  test('Renders sorting', () => {
    render(
      <MemoryRouter>
        <Congresspeople />
      </MemoryRouter>
    );
    const element = screen.getByTestId('sort');
    expect(element).toBeInTheDocument();
  });
});
