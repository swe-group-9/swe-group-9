import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import About from '../pages/about/AboutPage';

describe('About Page Testing', () => {
  test('Renders without crashing', () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const element = screen.getAllByText('About')[0];
    expect(element).toBeInTheDocument();
  });

  test('Renders team members', () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const element = screen.getByText('Team Members');
    expect(element).toBeInTheDocument();
  });

  test('Renders source code and API documentation', () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const element = screen.getByText('Source Code and API Documentation');
    expect(element).toBeInTheDocument();
  });

  test('Renders tools used', () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const element = screen.getByText('Tools Used');
    expect(element).toBeInTheDocument();
  });

  test('Renders APIs used', () => {
    render(
      <MemoryRouter>
        <About />
      </MemoryRouter>
    );
    const element = screen.getByText('APIs Used');
    expect(element).toBeInTheDocument();
  });
});
