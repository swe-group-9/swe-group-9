import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import NavigationBar from '../shared/NavigationBar';

describe('Navigation Bar and Search Testing', () => {
  test('Renders navigation bar and site search box', () => {
    render(
      <MemoryRouter>
        <NavigationBar />
      </MemoryRouter>
    );
    const element = screen.getByTestId('navigation-and-search');
    expect(element).toBeInTheDocument();
  });
});
