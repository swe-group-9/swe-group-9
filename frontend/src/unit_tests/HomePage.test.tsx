import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Home from '../pages/home/HomePage';

describe('Home Page Testing', () => {
  test('Renders site motto', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const element = screen.getByText('Keeping Congress in check.');
    expect(element).toBeInTheDocument();
  });

  test('Renders site tagline', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const element = screen.getByText('Lobbying data on your politicians.');
    expect(element).toBeInTheDocument();
  });

  test('Renders congresspeople card description', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const element = screen.getByText(
      'Learn how much money your representative receives from various industries.'
    );
    expect(element).toBeInTheDocument();
  });

  test('Renders committees card description', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const element = screen.getByText(
      'Discover how much money a committee receives from different industries.'
    );
    expect(element).toBeInTheDocument();
  });

  test('Renders industries card description', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );
    const element = screen.getByText(
      'Find out which industries donate the most money to politicans and committees.'
    );
    expect(element).toBeInTheDocument();
  });
});
