import { MemoryRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Committees from '../pages/committees/CommitteesPage';

describe('Committees Page Testing', () => {
  test('Renders without crashing', async () => {
    render(
      <MemoryRouter>
        <Committees />
      </MemoryRouter>
    );
    const element = screen.getAllByText('Committees')[0];
    expect(element).toBeInTheDocument();
  });

  test('Renders searching', async () => {
    render(
      <MemoryRouter>
        <Committees />
      </MemoryRouter>
    );
    const element = screen.getByTestId('search');
    expect(element).toBeInTheDocument();
  });

  test('Renders filtering', async () => {
    render(
      <MemoryRouter>
        <Committees />
      </MemoryRouter>
    );
    const element = screen.getByTestId('filter');
    expect(element).toBeInTheDocument();
  });

  test('Renders sorting', async () => {
    render(
      <MemoryRouter>
        <Committees />
      </MemoryRouter>
    );
    const element = screen.getByTestId('sort');
    expect(element).toBeInTheDocument();
  });
});
