function getHighlightSearch(query: string): RegExp | string {
  // gets list of search terms from query strings
  const terms = query
    .replaceAll(/[-+~*<>,'"]/g, ' ') // removes SQL characters from string to prevent missing matches
    .replace(/[.?^${}()|[\]\\]/g, '\\$&') // escapes RegEx characters from the query
    .split(/\s+/) // splits on any whitespace
    .filter((x: string) => x.length !== 0); // removes any empty terms

  // returns an empty string if there are no search terms, otherwise returns regex expression matching any of them (ignoring case)
  return terms.length === 0 ? '' : RegExp(`(?:${terms.join('|')})`, 'i');
}

export { getHighlightSearch };
