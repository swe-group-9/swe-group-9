import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import {
  Typography,
  Box,
  Container,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
} from '@mui/material';
import IndustryAImage from '../../assets/images/industries/A.jpg';
import IndustryBImage from '../../assets/images/industries/B.jpg';
import IndustryCImage from '../../assets/images/industries/C.jpg';
import IndustryDImage from '../../assets/images/industries/D.jpg';
import IndustryEImage from '../../assets/images/industries/E.jpg';
import IndustryFImage from '../../assets/images/industries/F.jpg';
import IndustryHImage from '../../assets/images/industries/H.jpg';
import IndustryKImage from '../../assets/images/industries/K.jpg';
import IndustryMImage from '../../assets/images/industries/M.jpg';
import IndustryNImage from '../../assets/images/industries/N.jpg';
import IndustryPImage from '../../assets/images/industries/P.jpg';
import IndustryQImage from '../../assets/images/industries/Q.jpg';
import IndustryWImage from '../../assets/images/industries/W.jpg';
import IndustryYImage from '../../assets/images/industries/Y.jpg';
import IndustryZImage from '../../assets/images/industries/Z.jpg';
import { getIndustry, getNewsArticles } from '../../shared/APIRequests';

const styles = {
  pictureCard: {
    width: '95%',
    margin: 'auto',
  },

  pictureMedia: {
    height: 225,
    width: '100%',
    objectFit: 'cover',
  },

  list: {
    paddingTop: 5,
    paddingBottom: 0,
  },

  newsCard: {
    marginBottom: 15,
  },

  newsMedia: {
    height: '200px',
  },
} as const;

function getIndustryImage(sectorCode: string) {
  if (sectorCode === 'A') {
    return IndustryAImage;
  } else if (sectorCode === 'B') {
    return IndustryBImage;
  } else if (sectorCode === 'C') {
    return IndustryCImage;
  } else if (sectorCode === 'D') {
    return IndustryDImage;
  } else if (sectorCode === 'E') {
    return IndustryEImage;
  } else if (sectorCode === 'F') {
    return IndustryFImage;
  } else if (sectorCode === 'H') {
    return IndustryHImage;
  } else if (sectorCode === 'K') {
    return IndustryKImage;
  } else if (sectorCode === 'M') {
    return IndustryMImage;
  } else if (sectorCode === 'N') {
    return IndustryNImage;
  } else if (sectorCode === 'P') {
    return IndustryPImage;
  } else if (sectorCode === 'Q') {
    return IndustryQImage;
  } else if (sectorCode === 'W') {
    return IndustryWImage;
  } else if (sectorCode === 'Y') {
    return IndustryYImage;
  } else {
    return IndustryZImage;
  }
}

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const Industry = () => {
  const ind_id = useParams().id || '';

  const [industryData, setIndustryData] = useState<any>([]);
  const [newsArticles, setNewsArticles] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [loadedData, setLoadedData] = useState(false);
  const [loadedNews, setLoadedNews] = useState(false);

  useEffect(() => {
    getIndustry(ind_id).then((response: any) => {
      setIndustryData(response);
      setFetched(true);
      setLoadedData(true);
    });
  }, []);

  useEffect(() => {
    if (fetched) {
      const topic = industryData.name.replace(/[^a-zA-Z ]/g, '');
      getNewsArticles(topic).then((response: any) => {
        if (response['page_size'] === 3) {
          setNewsArticles(response['articles']);
          setLoadedNews(true);
        } else {
          getNewsArticles('industries').then((response: any) => {
            if (response['page_size'] === 3) {
              setNewsArticles(response['articles']);
              setLoadedNews(true);
            }
          });
        }
      });
    }
  }, [fetched]);

  const industryImage = getIndustryImage(ind_id.charAt(0));

  let content = null;
  if (loadedData) {
    content = (
      <Box>
        <Typography gutterBottom variant="h4" component="div" pt={2}>
          {industryData?.name || ''}
        </Typography>

        <Container>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Card sx={{ maxWidth: 352 }} style={styles.pictureCard}>
                <CardActionArea disableRipple>
                  <CardMedia component="img" image={industryImage} style={styles.pictureMedia} />
                </CardActionArea>
              </Card>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography>
                  <b>Sector: </b>
                  {industryData?.sector || ''}
                </Typography>
                <Typography>
                  <b>Subindustries: </b>
                </Typography>
                <ul style={{ marginTop: 0, marginBottom: 0 }}>
                  {industryData?.categories?.map((category: any) => (
                    <li key={category}>{category}</li>
                  ))}
                </ul>
                <Typography>
                  <b>Top Party: </b>
                  {industryData?.party_most_contributed || ''}
                </Typography>
                <Typography>
                  <b>Top State: </b>
                  {industryData?.state_most_contributed || ''}
                </Typography>
                <Typography>
                  <b>Total Amount Contributed: </b>$
                  {numberWithCommas(industryData?.total_contributed || 0)}
                </Typography>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 6, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Top Congresspeople
                </Typography>
                <ol>
                  {industryData?.top_candidates_contributed?.map((congressperson: any) => (
                    <li style={styles.list} key={congressperson.cp_id}>
                      <Link
                        to={`/congresspeople/${congressperson.cp_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {congressperson.name}
                      </Link>
                      : ${numberWithCommas(congressperson.amount)}
                    </li>
                  )) || ''}
                </ol>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Top Committees
                </Typography>
                <ol>
                  {industryData?.top_committees_contributed?.map((committee: any) => (
                    <li style={styles.list} key={committee.comm_id}>
                      <Link
                        to={`/committees/${committee.comm_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {committee.name}
                      </Link>
                      : ${numberWithCommas(committee.amount)}
                    </li>
                  )) || ''}
                </ol>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Top Contributors
                </Typography>
                <ol>
                  {industryData?.top_contributors?.map((contributor: any) => (
                    <li style={styles.list} key={contributor.name}>
                      {contributor.name}: ${numberWithCommas(contributor.amount)}
                    </li>
                  )) || ''}
                </ol>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                sx={{
                  textAlign: 'left',
                }}
              >
                <Typography pt={2} pb={1} variant="h5" component="div" gutterBottom>
                  News articles
                </Typography>
                {loadedNews ? (
                  <Box display="flex" alignItems="center" flexDirection="column">
                    {newsArticles.map((article, index) => (
                      <a
                        key={newsArticles[index]['link']}
                        href={`${newsArticles[index]['link']}`}
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{ textDecoration: 'none' }}
                      >
                        <Card sx={{ maxWidth: 345 }} style={styles.newsCard}>
                          <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia
                              component="img"
                              image={`${newsArticles[index]['media']}`}
                              style={styles.newsMedia}
                            />
                            <CardContent>
                              <Typography gutterBottom variant="subtitle1" component="div">
                                {`${newsArticles[index]['title']}`}
                              </Typography>
                              <Typography gutterBottom variant="caption" component="div">
                                {`${newsArticles[index]['clean_url']}`}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </a>
                    ))}
                  </Box>
                ) : (
                  <CircularProgress />
                )}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default Industry;
