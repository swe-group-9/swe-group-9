import { Box, Typography } from '@mui/material';
import ModelTable from './IndustriesModelTable';

const Industries = () => {
  return (
    <Box>
      <Typography id="industriesPageTitle" gutterBottom variant="h3" component="div" pt={2}>
        Industries
      </Typography>
      <ModelTable />
    </Box>
  );
};

export default Industries;
