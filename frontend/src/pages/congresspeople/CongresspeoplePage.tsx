import { Box, Typography } from '@mui/material';
import ModelGrid from './CongresspeopleModelGrid';

const Congresspeople = () => {
  return (
    <Box>
      <Typography id="congresspeoplePageTitle" gutterBottom variant="h3" component="div" pt={2}>
        Congresspeople
      </Typography>
      <ModelGrid />
    </Box>
  );
};

export default Congresspeople;
