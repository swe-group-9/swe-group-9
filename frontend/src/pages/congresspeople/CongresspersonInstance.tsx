import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import {
  Typography,
  Box,
  Container,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
} from '@mui/material';
import { getCongressperson, getNewsArticles } from '../../shared/APIRequests';

const styles = {
  pictureCard: {
    width: '95%',
    margin: 'auto',
  },

  pictureMedia: {
    height: 280,
    width: '100%',
    objectFit: 'cover',
  },

  list: {
    paddingTop: 5,
    paddingBottom: 0,
  },

  newsCard: {
    marginBottom: 15,
  },

  newsMedia: {
    height: '200px',
  },
} as const;

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const Congressperson = () => {
  const cp_id = useParams().id || '';

  const [congresspersonData, setCongresspersonData] = useState<any>([]);
  const [newsArticles, setNewsArticles] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [loadedData, setLoadedData] = useState(false);
  const [loadedNews, setLoadedNews] = useState(false);

  useEffect(() => {
    getCongressperson(cp_id).then((response: any) => {
      setCongresspersonData(response);
      setFetched(true);
      setLoadedData(true);
    });
  }, []);

  useEffect(() => {
    if (fetched) {
      getNewsArticles(congresspersonData.name).then((response: any) => {
        if (response['page_size'] === 3) {
          setNewsArticles(response['articles']);
          setLoadedNews(true);
        } else {
          const topic = 'united states ' + congresspersonData.chamber;
          getNewsArticles(topic).then((response: any) => {
            if (response['page_size'] === 3) {
              setNewsArticles(response['articles']);
              setLoadedNews(true);
            }
          });
        }
      });
    }
  }, [fetched]);

  let content = null;
  if (loadedData) {
    content = (
      <Box>
        <Typography gutterBottom variant="h4" component="div" pt={2}>
          {congresspersonData.name}
        </Typography>
        <Typography gutterBottom variant="h6" component="div">
          {congresspersonData.title}
        </Typography>

        <Container>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={3}>
              <Card sx={{ maxWidth: 345 }} style={styles.pictureCard}>
                <CardActionArea disableRipple>
                  <CardMedia
                    component="img"
                    image={congresspersonData.image_url}
                    style={styles.pictureMedia}
                  />
                </CardActionArea>
              </Card>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography>{congresspersonData.description}</Typography>
                <Typography pt={2}>
                  <b>Chamber: </b>
                  {congresspersonData.chamber.charAt(0).toUpperCase() +
                    congresspersonData.chamber.substr(1).toLowerCase()}
                </Typography>
                <Typography>
                  <b>State: </b>
                  {congresspersonData.state}
                </Typography>
                <Typography>
                  <b>Party: </b>
                  {congresspersonData.political_party}
                </Typography>
                <Typography>
                  <b>Gender: </b>
                  {congresspersonData.gender}
                </Typography>
                <Typography>
                  <b>Phone: </b>
                  <a href={`tel:${congresspersonData.phone}`} style={{ textDecoration: 'none' }}>
                    {congresspersonData.phone}
                  </a>
                </Typography>
                <Typography>
                  <b>Website: </b>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`${congresspersonData.url}`}
                    style={{ textDecoration: 'none' }}
                  >
                    {congresspersonData.url
                      .replace(/(^\w+:|^)\/\//, '')
                      .replace(/^www./, '')
                      .replace(/\/+$/, '')}
                  </a>
                </Typography>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 6, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Committee Membership
                </Typography>
                <ul>
                  {congresspersonData?.committees?.map((committee: any) => (
                    <li style={styles.list} key={committee.comm_id}>
                      <Link
                        to={`/committees/${committee.comm_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {committee.name}
                      </Link>
                    </li>
                  )) || ''}
                </ul>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Contribution by Industries
                </Typography>
                <ol>
                  {congresspersonData.major_contributing_industries.map((industry: any) => (
                    <li style={styles.list} key={industry.ind_id}>
                      <Link
                        to={`/industries/${industry.ind_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {industry.name}
                      </Link>
                      : ${numberWithCommas(industry.amount)}
                    </li>
                  ))}
                </ol>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Top Contributors
                </Typography>
                <ol>
                  {congresspersonData.top_contributors.map((contributor: any) => (
                    <li style={styles.list} key={contributor.name}>
                      {contributor.name}: ${numberWithCommas(contributor.amount)}
                    </li>
                  ))}
                </ol>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <TwitterTimelineEmbed
                  sourceType="profile"
                  screenName={congresspersonData.twitter_account}
                  options={{ height: 600 }}
                />
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                sx={{
                  textAlign: 'left',
                }}
              >
                <Typography pt={2} pb={1} variant="h5" component="div" gutterBottom>
                  News articles
                </Typography>
                {loadedNews ? (
                  <Box display="flex" alignItems="center" flexDirection="column">
                    {newsArticles.map((article, index) => (
                      <a
                        key={newsArticles[index]['link']}
                        href={`${newsArticles[index]['link']}`}
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{ textDecoration: 'none' }}
                      >
                        <Card sx={{ maxWidth: 345 }} style={styles.newsCard}>
                          <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia
                              component="img"
                              image={`${newsArticles[index]['media']}`}
                              style={styles.newsMedia}
                            />
                            <CardContent>
                              <Typography gutterBottom variant="subtitle1" component="div">
                                {`${newsArticles[index]['title']}`}
                              </Typography>
                              <Typography gutterBottom variant="caption" component="div">
                                {`${newsArticles[index]['clean_url']}`}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </a>
                    ))}
                  </Box>
                ) : (
                  <CircularProgress />
                )}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default Congressperson;
