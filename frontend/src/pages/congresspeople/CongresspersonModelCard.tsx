import { Link } from 'react-router-dom';
import { Highlight } from 'react-highlighter-ts';
import { Grid, Typography, Card, CardContent, CardMedia, CardActionArea } from '@mui/material';
import { states, parties } from './CongresspeopleFilterInfo';
import { getHighlightSearch } from '../../shared/HighlightSearch';

const styles = {
  card: {
    height: '100%',
    width: '95%',
    margin: 'auto',
  },

  media: {
    height: 280,
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const ModelCard = (props: any) => {
  const { cp_id, image_url, name, chamber, state, political_party, currently_in_office, query } =
    props;

  return (
    <Grid item key={cp_id} xs={1} sm={4} md={3}>
      <Link to={`/congresspeople/${cp_id}`} style={{ textDecoration: 'none' }}>
        <Card sx={{ maxWidth: 345 }} style={styles.card}>
          <CardActionArea style={{ outline: 'none' }}>
            <CardMedia component="img" image={image_url} style={styles.media} />
            <CardContent>
              <Typography gutterBottom variant="h6" component="div">
                <Highlight search={getHighlightSearch(query)}>{name}</Highlight>
              </Typography>
              <Typography gutterBottom variant="body1" component="div">
                <Highlight search={getHighlightSearch(query)}>{chamber}</Highlight>
              </Typography>
              <Typography gutterBottom variant="body1" component="div">
                <Highlight search={getHighlightSearch(query)}>
                  {states.find((i) => i.value === state)?.name}
                </Highlight>
              </Typography>
              <Typography gutterBottom variant="body1" component="div">
                <Highlight search={getHighlightSearch(query)}>
                  {parties.find((i) => i.value === political_party)?.name}
                </Highlight>
              </Typography>
              {currently_in_office ? (
                <Typography gutterBottom variant="body1" component="div">
                  In-office
                </Typography>
              ) : (
                <Typography gutterBottom variant="body1" component="div">
                  Out-of-office
                </Typography>
              )}
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
};

export default ModelCard;
