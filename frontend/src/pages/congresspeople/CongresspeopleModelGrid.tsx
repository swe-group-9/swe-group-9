import { useEffect, useState, MouseEventHandler } from 'react';
import {
  Box,
  Grid,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Radio,
  ListItemText,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import ModelCard from './CongresspersonModelCard';
import SearchBar from '../../shared/SearchBar';
import Pagination from '../../shared/Pagination';
import { getCongresspeople } from '../../shared/APIRequests';
import { chambers, parties, states, officeStatuses, sortOptions } from './CongresspeopleFilterInfo';

const ModelGrid = () => {
  const [congresspeopleData, setCongresspeopleData] = useState<any[]>([]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [chamber, setChamber] = useState('');
  const [party, setParty] = useState('');
  const [state, setState] = useState('');
  const [officeStatus, setOfficeStatus] = useState('');
  const [sort, setSort] = useState('');
  const [sortParam, setSortParam] = useState('');
  const [order, setOrder] = useState('');
  const [query, setQuery] = useState('');
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getCongresspeople(page + 1, chamber, party, state, officeStatus, sortParam, order, query).then(
      (response: any) => {
        setCongresspeopleData(response['page']);
        setCount(response['count']);
        setLoaded(true);
      }
    );
  }, [page, chamber, party, state, officeStatus, sortParam, order, query]);

  const handleChamberChange = (event: any) => {
    setChamber(event.target.value);
  };

  const handlePartyChange = (event: any) => {
    setParty(event.target.value);
  };

  const handleStateChange = (event: any) => {
    setState(event.target.value);
  };

  const handleOfficeStatusChange = (event: any) => {
    setOfficeStatus(event.target.value);
  };

  const handleSortChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    setSort(value);
    if (value == 'name-asc') {
      setSortParam('name');
      setOrder('');
    } else if (value == 'name-desc') {
      setSortParam('name');
      setOrder('true');
    }
  };

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    if (value == '') {
      setQuery('');
    } else {
      setQuery(value);
    }
  };

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setChamber('');
    setParty('');
    setState('');
    setOfficeStatus('');
    setSort('');
    setSortParam('');
    setOrder('');
    setQuery('');
  };

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} />
        </Grid>

        <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl id="chamberFilter" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Chamber</InputLabel>
              <Select
                data-testid="filter"
                defaultValue=""
                value={chamber}
                onChange={handleChamberChange}
                renderValue={(selected) => chambers.find((i) => i.value === selected)?.name}
              >
                {chambers.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={chamber == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Party</InputLabel>
              <Select
                defaultValue=""
                value={party}
                onChange={handlePartyChange}
                renderValue={(selected) => parties.find((i) => i.value === selected)?.name}
              >
                {parties.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={party == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>State</InputLabel>
              <Select
                defaultValue=""
                value={state}
                onChange={handleStateChange}
                renderValue={(selected) => states.find((i) => i.value === selected)?.name}
              >
                {states.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={state == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Office status</InputLabel>
              <Select
                defaultValue=""
                value={officeStatus}
                onChange={handleOfficeStatusChange}
                renderValue={(selected) => officeStatuses.find((i) => i.value === selected)?.name}
              >
                {officeStatuses.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={officeStatus == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 130 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sort}
                onChange={handleSortChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Grid>
      </Grid>

      {loaded ? (
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 2, sm: 8, md: 16 }}
          p={3}
          justifyContent="center"
        >
          {congresspeopleData.map((congressperson) => (
            <ModelCard
              key={congressperson.cp_id}
              cp_id={congressperson.cp_id}
              image_url={congressperson.image_url}
              name={congressperson.name}
              chamber={
                congressperson.chamber.charAt(0).toUpperCase() +
                congressperson.chamber.substr(1).toLowerCase()
              }
              state={congressperson.state}
              political_party={congressperson.political_party}
              currently_in_office={congressperson.currently_in_office}
              query={query}
            />
          ))}
        </Grid>
      ) : (
        <CircularProgress />
      )}

      <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ModelGrid;
