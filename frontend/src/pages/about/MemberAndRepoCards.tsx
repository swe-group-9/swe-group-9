import { useEffect, useState } from 'react';
import CommitIcon from '@mui/icons-material/Commit';
import ListAltIcon from '@mui/icons-material/ListAlt';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import {
  Box,
  Container,
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
} from '@mui/material';
import GitLabLogo from '../../assets/logos/gitlab.svg';
import PostmanLogo from '../../assets/logos/postman.svg';
import MemberCard from './components/MemberCard';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { teamInfo } from './components/TeamInfo';
import { getGitlabInfo } from './components/GitLabStats';

const styles = {
  card: {
    height: '100%',
  },

  media: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const MemberAndRepoCards = () => {
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);
  const [totalTests, setTotalTests] = useState(0);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (teamList === undefined || teamList.length === 0) {
        const gitlabInfo = await getGitlabInfo();
        setTotalCommits(gitlabInfo.totalCommits);
        setTotalIssues(gitlabInfo.totalIssues);
        setTotalTests(gitlabInfo.totalTests);
        // @ts-ignore
        setTeamList(gitlabInfo.teamInfo);
        setLoaded(true);
      }
    };
    fetchData();
  }, [teamList]);

  return (
    <Box>
      <Container>
        {loaded ? (
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 9 }}
            p={3}
            justifyContent="center"
          >
            {teamList.map((member) => {
              const { name, username, picture_path, role, bio, commits, issues, tests } = member;
              return (
                <MemberCard
                  key={name}
                  username={username}
                  name={name}
                  picture_path={picture_path}
                  role={role}
                  bio={bio}
                  commits={commits}
                  issues={issues}
                  tests={tests}
                />
              );
            })}
          </Grid>
        ) : (
          <CircularProgress />
        )}
      </Container>

      <Typography gutterBottom variant="h4" component="div" pt={2}>
        Source Code and API Documentation
      </Typography>
      <Container>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 2, sm: 8, md: 12 }}
          p={3}
          justifyContent="center"
        >
          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://gitlab.com/catchingcongress/catchingcongress"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${GitLabLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      GitLab Repository
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body1" color="text.secondary" p={2} component="div">
                        <div
                          style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                            flexWrap: 'wrap',
                          }}
                        >
                          <CommitIcon style={{ position: 'relative', minWidth: '39px' }} />
                          {totalCommits}
                        </div>
                        <div
                          style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                            flexWrap: 'wrap',
                          }}
                        >
                          <ListAltIcon style={{ position: 'relative', minWidth: '39px' }} />
                          {totalIssues}
                        </div>
                        <div
                          style={{
                            display: 'inline-flex',
                            alignItems: 'center',
                            flexWrap: 'wrap',
                          }}
                        >
                          <CheckCircleOutlineIcon
                            style={{ position: 'relative', minWidth: '39px' }}
                          />
                          {totalTests}
                        </div>
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://documenter.getpostman.com/view/19746295/UVyrTbkF"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${PostmanLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Postman API
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}></Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default MemberAndRepoCards;
