import ProPublicaLogo from '../../assets/logos/propublica.svg';
import OpenSecretsLogo from '../../assets/logos/opensecrets.svg';
import OpenFECLogo from '../../assets/logos/openfec.svg';
import GoogleLogo from '../../assets/logos/google.svg';
import NewsCatcherLogo from '../../assets/logos/newscatcher.svg';
import TwitterLogo from '../../assets/logos/twitter.svg';
import MediaWikiLogo from '../../assets/logos/mediawiki.svg';
// import GoogleMapsLogo from '../../assets/logos/googlemaps.svg';
import {
  Container,
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
} from '@mui/material';

const styles = {
  card: {
    height: '96%',
  },

  media: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
  },
} as const;

const APICards = () => {
  return (
    <Box>
      <Container>
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }} p={3}>
          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://projects.propublica.org/api-docs/congress-api/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${ProPublicaLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      ProPublica
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Legislative data about Congress
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.opensecrets.org/open-data/api"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${OpenSecretsLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      OpenSecrets
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Lobbying data about Congress
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://api.open.fec.gov/developers/"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${OpenFECLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      OpenFEC
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Campaign funding data
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://developers.google.com/knowledge-graph"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${GoogleLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Google Search
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Descriptions from the Knowledge Graph for instances
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://newscatcherapi.com/news-api"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${NewsCatcherLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      News Catcher
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        News articles for instances
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://developer.twitter.com/docs/twitter-api"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${TwitterLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Twitter
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Twitter feeds for instances
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://www.mediawiki.org/wiki/API:Main_page"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${MediaWikiLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      MediaWiki
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Images for congresspeople
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid>

          {/* <Grid item xs={1} sm={4} md={3}>
            <a
              href="https://developers.google.com/maps"
              target="_blank"
              rel="noopener noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <Card sx={{ maxWidth: 345 }} style={styles.card}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" image={`${GoogleMapsLogo}`} style={styles.media} />
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      Google Maps
                    </Typography>
                    <Box sx={{ minHeight: '10vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Map embeds for office locations
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </a>
          </Grid> */}
        </Grid>
      </Container>
    </Box>
  );
};

export default APICards;
