import { Link, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Highlight } from 'react-highlighter-ts';
import {
  Box,
  Typography,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  CircularProgress,
} from '@mui/material';
import ModelCard from '../congresspeople/CongresspersonModelCard';
import { getCongresspeople, getCommittees, getIndustries } from '../../shared/APIRequests';
import { getHighlightSearch } from '../../shared/HighlightSearch';

const Search = () => {
  const query = useParams().id || '';
  const emptyVal: any = null;

  const [congresspeopleData, setCongresspeopleData] = useState<any[]>([]);
  const [committeesData, setCommitteesData] = useState<any[]>([]);
  const [industriesData, setIndustriesData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getCongresspeople(1, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, query).then(
      (response: any) => {
        setCongresspeopleData(response['page']);
        getCommittees(1, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, query).then(
          (response: any) => {
            setCommitteesData(response['page']);
            getIndustries(1, emptyVal, emptyVal, emptyVal, emptyVal, emptyVal, query).then(
              (response: any) => {
                setIndustriesData(response['page']);
                setLoaded(true);
              }
            );
          }
        );
      }
    );
  }, [query]);

  return (
    <Box pb={3}>
      <Typography id="congresspeopleSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
        Congresspeople
      </Typography>
      {loaded ? (
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 2, sm: 8, md: 16 }}
          p={3}
          justifyContent="center"
        >
          {congresspeopleData.map((congressperson) => (
            <ModelCard
              key={congressperson.cp_id}
              cp_id={congressperson.cp_id}
              image_url={congressperson.image_url}
              name={congressperson.name}
              chamber={
                congressperson.chamber.charAt(0).toUpperCase() +
                congressperson.chamber.substr(1).toLowerCase()
              }
              state={congressperson.state}
              political_party={congressperson.political_party}
              currently_in_office={congressperson.currently_in_office}
              query={query}
            />
          ))}
        </Grid>
      ) : (
        <CircularProgress />
      )}

      <Typography id="committeesSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
        Committees
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <TableContainer component={Paper} style={{ width: '80%' }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div">Name</TableCell>
                  <TableCell component="div" align="right">
                    Chamber
                  </TableCell>
                  <TableCell component="div" align="right">
                    Committee type
                  </TableCell>
                  <TableCell component="div" align="right">
                    Chair party
                  </TableCell>
                  <TableCell component="div" align="right">
                    Number of subcommittees
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {committeesData.map((committee) => (
                  <TableRow
                    hover
                    key={committee.comm_id}
                    component={Link}
                    to={`/committees/${committee.comm_id}`}
                    style={{ textDecoration: 'none' }}
                  >
                    <TableCell component="div" scope="row">
                      <Highlight search={getHighlightSearch(query)}>{committee.name}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      {committee.chamber}
                    </TableCell>
                    {committee.is_primary ? (
                      <TableCell component="div" align="right">
                        primary
                      </TableCell>
                    ) : (
                      <TableCell component="div" align="right">
                        subcommittee
                      </TableCell>
                    )}
                    <TableCell component="div" align="right">
                      D
                    </TableCell>
                    <TableCell component="div" align="right">
                      {committee.num_subcommittees}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <CircularProgress />
        )}
      </Grid>

      <Typography id="industriesSectionTitle" gutterBottom variant="h3" component="div" pt={2}>
        Industries
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <TableContainer component={Paper} style={{ width: '80%' }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div">Name</TableCell>
                  <TableCell component="div" align="right">
                    Sector
                  </TableCell>
                  <TableCell component="div" align="right">
                    Top chamber
                  </TableCell>
                  <TableCell component="div" align="right">
                    Top party
                  </TableCell>
                  <TableCell component="div" align="right">
                    Amount contributed
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {industriesData.map((industry) => (
                  <TableRow
                    hover
                    key={industry.ind_id}
                    component={Link}
                    to={`/industries/${industry.ind_id}`}
                    style={{ textDecoration: 'none' }}
                  >
                    <TableCell component="div" scope="row">
                      <Highlight search={getHighlightSearch(query)}>{industry.name}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      <Highlight search={getHighlightSearch(query)}>{industry.sector}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      {industry.chamber_most_contributed}
                    </TableCell>
                    <TableCell component="div" align="right">
                      {industry.party_most_contributed}
                    </TableCell>
                    <TableCell component="div" align="right">
                      ${industry.total_contributed}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default Search;
