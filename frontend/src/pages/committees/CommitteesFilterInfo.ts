const chambers = [
  {
    name: 'House',
    value: 'house',
  },
  {
    name: 'Senate',
    value: 'senate',
  },
  {
    name: 'Joint',
    value: 'joint',
  },
];

const types = [
  {
    name: 'Primary',
    value: '1',
  },
  {
    name: 'Subcommittee',
    value: '0',
  },
];

const associatedCommittees = [
  {
    name: 'House Committee on Agriculture',
    value: 'HSAG',
  },
  {
    name: 'House Committee on Appropriations',
    value: 'HSAP',
  },
  {
    name: 'House Committee on Armed Services',
    value: 'HSAS',
  },
  {
    name: 'House Committee on Financial Services',
    value: 'HSBA',
  },
  {
    name: 'House Committee on Education and Labor',
    value: 'HSED',
  },
  {
    name: 'House Committee on Foreign Affairs',
    value: 'HSFA',
  },
  {
    name: 'House Committee on Oversight and Reform',
    value: 'HSGO',
  },
  {
    name: 'House Committee on Administration',
    value: 'HSHA',
  },
  {
    name: 'House Committee on Homeland Security',
    value: 'HSHM',
  },
  {
    name: 'House Committee on Energy and Commerce',
    value: 'HSIF',
  },
  {
    name: 'Permanent Select House Committee on Intelligence',
    value: 'HSIG',
  },
  {
    name: 'House Committee on Natural Resources',
    value: 'HSII',
  },
  {
    name: 'House Committee on the Judiciary',
    value: 'HSJU',
  },
  {
    name: 'House Committee on Transportation and Infrastructure',
    value: 'HSPW',
  },
  {
    name: 'House Committee on Rules',
    value: 'HSRU',
  },
  {
    name: 'House Committee on Small Business',
    value: 'HSSM',
  },
  {
    name: 'House Committee on Science, Space, and Technology',
    value: 'HSSY',
  },
  {
    name: "House Committee on Veterans' Affairs",
    value: 'HSVR',
  },
  {
    name: 'House Committee on Ways and Means',
    value: 'HSWM',
  },
  {
    name: 'Senate Committee on Agriculture, Nutrition, and Forestry',
    value: 'SSAF',
  },
  {
    name: 'Senate Committee on Appropriations',
    value: 'SSAP',
  },
  {
    name: 'Senate Committee on Armed Services',
    value: 'SSAS',
  },
  {
    name: 'Senate Committee on Banking, Housing, and Urban Affairs',
    value: 'SSBK',
  },
  {
    name: 'Senate Committee on Commerce, Science, and Transportation',
    value: 'SSCM',
  },
  {
    name: 'Senate Committee on Energy and Natural Resources',
    value: 'SSEG',
  },
  {
    name: 'Senate Committee on Environment and Public Works',
    value: 'SSEV',
  },
  {
    name: 'Senate Committee on Finance',
    value: 'SSFI',
  },
  {
    name: 'Senate Committee on Foreign Relations',
    value: 'SSFR',
  },
  {
    name: 'Senate Committee on Homeland Security and Governmental Affairs',
    value: 'SSGA',
  },
  {
    name: 'Senate Committee on Health, Education, Labor, and Pensions',
    value: 'SSHR',
  },
  {
    name: 'Senate Committee on the Judiciary',
    value: 'SSJU',
  },
];

const sortOptions = [
  {
    name: 'Name (A-Z)',
    value: 'name-asc',
  },
  {
    name: 'Name (Z-A)',
    value: 'name-desc',
  },
  {
    name: 'Subcommittees (Asc.)',
    value: 'subcommittees-asc',
  },
  {
    name: 'Subcommittees (Desc.)',
    value: 'subcommittees-desc',
  },
];

export { chambers, types, associatedCommittees, sortOptions };
