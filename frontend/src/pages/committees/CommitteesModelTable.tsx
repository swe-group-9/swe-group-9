import { Link } from 'react-router-dom';
import { useEffect, useState, MouseEventHandler } from 'react';
import { Highlight } from 'react-highlighter-ts';
import {
  Box,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  InputLabel,
  Select,
  MenuItem,
  FormControl,
  Radio,
  ListItemText,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@mui/material';
import ClearAllIcon from '@mui/icons-material/ClearAll';
import SearchBar from '../../shared/SearchBar';
import Pagination from '../../shared/Pagination';
import { getCommittees } from '../../shared/APIRequests';
import { chambers, types, associatedCommittees, sortOptions } from './CommitteesFilterInfo';
import { getHighlightSearch } from '../../shared/HighlightSearch';

const ModelTable = () => {
  const [committeesData, setCommitteesData] = useState<any[]>([]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [chamber, setChamber] = useState('');
  const [type, setType] = useState('');
  const [associatedCommittee, setAssociatedCommittee] = useState('');
  const [sort, setSort] = useState('');
  const [sortParam, setSortParam] = useState('');
  const [order, setOrder] = useState('');
  const [query, setQuery] = useState('');
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    getCommittees(page + 1, chamber, type, associatedCommittee, sortParam, order, query).then(
      (response: any) => {
        setCommitteesData(response['page']);
        setCount(response['count']);
        setLoaded(true);
      }
    );
  }, [page, chamber, type, associatedCommittee, sortParam, order, query]);

  const handleChamberChange = (event: any) => {
    setChamber(event.target.value);
  };

  const handleTypeChange = (event: any) => {
    setType(event.target.value);
  };

  const handleAssociatedCommitteeChange = (event: any) => {
    setAssociatedCommittee(event.target.value);
  };

  const handleSortChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    setSort(value);
    if (value == 'name-asc') {
      setSortParam('name');
      setOrder('');
    } else if (value == 'name-desc') {
      setSortParam('name');
      setOrder('true');
    } else if (value == 'subcommittees-asc') {
      setSortParam('num_subcommittees');
      setOrder('');
    } else if (value == 'subcommittees-desc') {
      setSortParam('num_subcommittees');
      setOrder('true');
    }
  };

  const handleQueryChange = (event: any) => {
    const value = event.target.value;
    setPage(0);
    if (value == '') {
      setQuery('');
    } else {
      setQuery(value);
    }
  };

  const handleClearAllClick: MouseEventHandler<HTMLButtonElement> = () => {
    setPage(0);
    setChamber('');
    setType('');
    setAssociatedCommittee('');
    setSort('');
    setSortParam('');
    setOrder('');
    setQuery('');
  };

  return (
    <Box>
      <Grid
        container
        spacing={0}
        pb={3}
        direction="column"
        alignItems="center"
        justifyContent="center"
      >
        <Grid id="searchBar" item xs={3}>
          <SearchBar query={query} onChange={handleQueryChange} />
        </Grid>

        <Grid item pl={2} xs={3}>
          <div
            style={{
              display: 'inline-flex',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            <FormControl id="chamberFilter" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Chamber</InputLabel>
              <Select
                data-testid="filter"
                defaultValue=""
                value={chamber}
                onChange={handleChamberChange}
                renderValue={(selected) => chambers.find((i) => i.value === selected)?.name}
              >
                {chambers.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={chamber == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Type</InputLabel>
              <Select
                value={type}
                defaultValue=""
                onChange={handleTypeChange}
                renderValue={(selected) => types.find((i) => i.value === selected)?.name}
              >
                {types.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={type == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: 180 }} variant="standard">
              <InputLabel>Assoc. committees</InputLabel>
              <Select
                value={associatedCommittee}
                defaultValue=""
                onChange={handleAssociatedCommitteeChange}
                renderValue={(selected) =>
                  associatedCommittees.find((i) => i.value === selected)?.name
                }
              >
                {associatedCommittees.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    <Radio checked={associatedCommittee == element.value} />
                    <ListItemText primary={element.name} />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl id="sortMenu" sx={{ m: 1, width: 140 }} variant="standard">
              <InputLabel>Sort by</InputLabel>
              <Select
                data-testid="sort"
                defaultValue=""
                value={sort}
                onChange={handleSortChange}
                renderValue={(selected) => sortOptions.find((i) => i.value === selected)?.name}
              >
                {sortOptions.map((element, key) => (
                  <MenuItem key={key} value={element.value}>
                    {element.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: 12 }}>
              <Tooltip title="Clear all">
                <IconButton aria-label="clear-all" onClick={handleClearAllClick}>
                  <ClearAllIcon />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Grid>
      </Grid>

      <Grid container justifyContent="center">
        {loaded ? (
          <TableContainer component={Paper} style={{ width: '80%' }}>
            <Table component="div" sx={{ minWidth: 650 }}>
              <TableHead component="div">
                <TableRow component="div">
                  <TableCell component="div">Name</TableCell>
                  <TableCell component="div" align="right">
                    Chamber
                  </TableCell>
                  <TableCell component="div" align="right">
                    Committee type
                  </TableCell>
                  <TableCell component="div" align="right">
                    Chair party
                  </TableCell>
                  <TableCell component="div" align="right">
                    Number of subcommittees
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody component="div">
                {committeesData.map((committee) => (
                  <TableRow
                    hover
                    key={committee.comm_id}
                    component={Link}
                    to={`/committees/${committee.comm_id}`}
                    style={{ textDecoration: 'none' }}
                  >
                    <TableCell component="div" scope="row">
                      <Highlight search={getHighlightSearch(query)}>{committee.name}</Highlight>
                    </TableCell>
                    <TableCell component="div" align="right">
                      {committee.chamber}
                    </TableCell>
                    {committee.is_primary ? (
                      <TableCell component="div" align="right">
                        primary
                      </TableCell>
                    ) : (
                      <TableCell component="div" align="right">
                        subcommittee
                      </TableCell>
                    )}
                    <TableCell component="div" align="right">
                      D
                    </TableCell>
                    <TableCell component="div" align="right">
                      {committee.num_subcommittees}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <CircularProgress />
        )}
      </Grid>
      <Grid item xs={12}>
        <Grid container alignItems="center" justifyContent="center">
          <Pagination page={page} setPage={setPage} count={count} />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ModelTable;
