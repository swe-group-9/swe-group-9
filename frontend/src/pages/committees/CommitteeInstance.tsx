import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import {
  Typography,
  Box,
  Container,
  Grid,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  CircularProgress,
} from '@mui/material';
import HouseSeal from '../../assets/logos/house.svg';
import SenateSeal from '../../assets/logos/senate.svg';
import JointSeal from '../../assets/logos/joint.svg';
import { getCommittee, getNewsArticles } from '../../shared/APIRequests';

const styles = {
  pictureCard: {
    width: '95%',
    margin: 'auto',
  },

  pictureMedia: {
    objectFit: 'cover',
  },

  list: {
    paddingTop: 5,
    paddingBottom: 0,
  },

  newsCard: {
    marginBottom: 15,
  },

  newsMedia: {
    height: '200px',
  },
} as const;

function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

const Committee = () => {
  const comm_id = useParams().id || '';

  const [committeeData, setCommitteeData] = useState<any>([]);
  const [committeeName, setCommitteeName] = useState('');
  const [committeeType, setCommitteeType] = useState(false);
  const [newsArticles, setNewsArticles] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [loadedData, setLoadedData] = useState(false);
  const [loadedNews, setLoadedNews] = useState(false);

  useEffect(() => {
    getCommittee(comm_id).then((response: any) => {
      setCommitteeData(response);
      setCommitteeName(response.name);
      setCommitteeType(response.is_primary);
      setFetched(true);
    });
  }, []);

  useEffect(() => {
    if (fetched) {
      if (!committeeType) {
        getCommittee(committeeData.associated_committees[0].comm_id).then((response: any) => {
          setCommitteeData(response);
          setLoadedData(true);
        });
      } else {
        setLoadedData(true);
      }

      getNewsArticles(committeeData.name).then((response: any) => {
        if (response['page_size'] === 3) {
          setNewsArticles(response['articles']);
          setLoadedNews(true);
        } else {
          const topic = 'united states ' + committeeData.chamber;
          getNewsArticles(topic).then((response: any) => {
            if (response['page_size'] === 3) {
              setNewsArticles(response['articles']);
              setLoadedNews(true);
            }
          });
        }
      });
    }
  }, [fetched]);

  let seal = JointSeal;
  let twitterAccount = 'congressdotgov';
  if (committeeData.chamber === 'house') {
    seal = HouseSeal;
    twitterAccount = 'HouseDemocrats';
  } else if (committeeData.chamber === 'senate') {
    seal = SenateSeal;
    twitterAccount = 'SenateDems';
  }

  let content = null;
  if (loadedData) {
    content = (
      <Box>
        <Typography gutterBottom variant="h4" component="div" pt={2}>
          {committeeName}
        </Typography>

        <Container>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={1} sm={3} md={3}>
              <Card sx={{ maxWidth: 225 }} style={styles.pictureCard}>
                <CardActionArea disableRipple>
                  <CardMedia component="img" image={seal} style={styles.pictureMedia} />
                </CardActionArea>
              </Card>
            </Grid>

            <Grid item xs={1} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography>
                  <b>Chamber: </b>
                  {committeeData.chamber.charAt(0).toUpperCase() +
                    committeeData.chamber.substr(1).toLowerCase()}
                </Typography>
                {committeeType ? (
                  <Typography>
                    <b>Committee Type: </b> Primary
                  </Typography>
                ) : (
                  <Typography>
                    <b>Committee Type: </b> Subcommittee
                  </Typography>
                )}
                <Typography>
                  <b>Primary Chair: </b>
                  <Link
                    to={`/congresspeople/${committeeData?.chair?.cp_id || ''}`}
                    style={{ textDecoration: 'none' }}
                  >
                    {committeeData?.chair?.name || ''}
                  </Link>
                </Typography>
                <Typography>
                  <b>Primary Ranking Member: </b>
                  <Link
                    to={`/congresspeople/${committeeData?.ranking_member?.cp_id || ''}`}
                    style={{ textDecoration: 'none' }}
                  >
                    {committeeData?.ranking_member?.name || ''}
                  </Link>
                </Typography>
                <Typography>
                  <b>Website: </b>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`${committeeData.url}`}
                    style={{ textDecoration: 'none' }}
                  >
                    {committeeData.url
                      .replace(/(^\w+:|^)\/\//, '')
                      .replace(/^www./, '')
                      .replace(/\/+$/, '')}
                  </a>
                </Typography>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 6, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Primary Congresspeople
                </Typography>
                <ul>
                  {committeeData.members.map((congressperson: any) => (
                    <li style={styles.list} key={congressperson.cp_id}>
                      <Link
                        to={`/congresspeople/${congressperson.cp_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {congressperson.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Contribution by Industries
                </Typography>
                <ol>
                  {committeeData.major_contributing_industries.map((industry: any) => (
                    <li style={styles.list} key={industry.ind_id}>
                      <Link
                        to={`/industries/${industry.ind_id}`}
                        style={{ textDecoration: 'none' }}
                      >
                        {industry.name}
                      </Link>
                      : ${numberWithCommas(industry.amount)}
                    </li>
                  ))}
                </ol>
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <Typography variant="h5" component="div" gutterBottom>
                  Contributing Sectors
                </Typography>
                <ul>
                  {committeeData.sectors_recieved_money_from.map((sector: any) => (
                    <li style={styles.list} key={sector}>
                      {sector}
                    </li>
                  ))}
                </ul>
              </Box>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 2, sm: 8, md: 12 }}
            p={3}
            justifyContent="center"
          >
            <Grid item xs={2} sm={3} md={4}>
              <Box
                p={2}
                sx={{
                  boxShadow: 1,
                  borderRadius: 2,
                  textAlign: 'left',
                }}
              >
                <TwitterTimelineEmbed
                  sourceType="profile"
                  screenName={twitterAccount}
                  options={{ height: 600 }}
                />
              </Box>
            </Grid>

            <Grid item xs={2} sm={3} md={4}>
              <Box
                sx={{
                  textAlign: 'left',
                }}
              >
                <Typography pt={2} pb={1} variant="h5" component="div" gutterBottom>
                  News articles
                </Typography>
                {loadedNews ? (
                  <Box display="flex" alignItems="center" flexDirection="column">
                    {newsArticles.map((article, index) => (
                      <a
                        key={newsArticles[index]['link']}
                        href={`${newsArticles[index]['link']}`}
                        target="_blank"
                        rel="noopener noreferrer"
                        style={{ textDecoration: 'none' }}
                      >
                        <Card sx={{ maxWidth: 345 }} style={styles.newsCard}>
                          <CardActionArea style={{ outline: 'none' }}>
                            <CardMedia
                              component="img"
                              image={`${newsArticles[index]['media']}`}
                              style={styles.newsMedia}
                            />
                            <CardContent>
                              <Typography gutterBottom variant="subtitle1" component="div">
                                {`${newsArticles[index]['title']}`}
                              </Typography>
                              <Typography gutterBottom variant="caption" component="div">
                                {`${newsArticles[index]['clean_url']}`}
                              </Typography>
                            </CardContent>
                          </CardActionArea>
                        </Card>
                      </a>
                    ))}
                  </Box>
                ) : (
                  <CircularProgress />
                )}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    );
  }

  return <Box>{loadedData ? content : <CircularProgress />}</Box>;
};

export default Committee;
