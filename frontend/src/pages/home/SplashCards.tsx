import { Link } from 'react-router-dom';
import {
  Container,
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
} from '@mui/material';
import CongresspeopleImage from '../../assets/images/congresspeople_card.jpg';
import CommitteesImage from '../../assets/images/committees_card.jpg';
import IndustriesImage from '../../assets/images/industries_card.jpg';

const SplashCards = () => {
  return (
    <Box>
      <Container>
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, sm: 8, md: 12 }} p={3}>
          <Grid item xs={2} sm={4} md={4}>
            <Link to="/congresspeople" style={{ textDecoration: 'none' }}>
              <Card id="congresspeopleCard" sx={{ maxWidth: 345 }}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" height="200" image={CongresspeopleImage} />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      Congresspeople
                    </Typography>
                    <Box sx={{ minHeight: '7vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Learn how much money your representative receives from various industries.
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>

          <Grid item xs={2} sm={4} md={4}>
            <Link to="/committees" style={{ textDecoration: 'none' }}>
              <Card id="committeesCard" sx={{ maxWidth: 345 }}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" height="200" image={CommitteesImage} />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      Committees
                    </Typography>
                    <Box sx={{ minHeight: '7vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Discover how much money a committee receives from different industries.
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>

          <Grid item xs={2} sm={4} md={4}>
            <Link to="/industries" style={{ textDecoration: 'none' }}>
              <Card id="industriesCard" sx={{ maxWidth: 345 }}>
                <CardActionArea style={{ outline: 'none' }}>
                  <CardMedia component="img" height="200" image={IndustriesImage} />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      Industries
                    </Typography>
                    <Box sx={{ minHeight: '7vh' }}>
                      <Typography variant="body2" color="text.secondary">
                        Find out which industries donate the most money to politicans and
                        committees.
                      </Typography>
                    </Box>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default SplashCards;
