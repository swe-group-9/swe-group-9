import { Box } from '@mui/material';
import Splash from './Splash';
import SplashCards from './SplashCards';

const Home = () => {
  return (
    <Box>
      <Box>
        <Splash />
        <SplashCards />
      </Box>
    </Box>
  );
};

export default Home;
