import { Box, Typography } from '@mui/material';
import Image from '../../assets/images/us_capitol.jpg';

const styles = {
  splashImage: {
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${Image})`,
    height: '80vh',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
  },

  splashText: {
    textAlign: 'center',
    position: 'absolute',
    top: '50%',
    left: '50%',
    color: '#fff',
    transform: 'translate(-50%, -50%)',
  },
} as const;

const Splash = () => {
  return (
    <Box>
      <Box style={styles.splashImage}>
        <Box style={styles.splashText}>
          <Typography id="motto" variant="h3">
            Keeping Congress in check.
          </Typography>
          <Box style={{ width: '10px', height: '10px' }}></Box>
          <Typography id="tagline" variant="h6">
            Lobbying data on your politicians.
          </Typography>
          <Box style={{ width: '40px', height: '50px' }}></Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Splash;
