import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { Bar, BarChart, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';
import { getIndustries } from '../../../shared/APIRequests';

function addContributions(industriesData: any, tempData: any[]) {
  for (let industry = 0; industry < industriesData.length; industry++) {
    const industryContribution = {
      name: industriesData[industry].name,
      id: industriesData[industry].ind_id,
      Amount: industriesData[industry]?.total_contributed || 0,
    };
    if (industryContribution.Amount != 0) {
      tempData.push(industryContribution);
    }
  }
}

const IndustriesVisualization = () => {
  const [data, setData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    const tempData: any[] = [];
    Promise.all(
      Array.from(Array(4).keys()).map((i) =>
        getIndustries(i + 1, '', '', '', 'total_contributed', 'true', '')
      )
    ).then((responses: any[]) => {
      responses.forEach((response) => addContributions(response['page'], tempData));
      setData(tempData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Industry Contributions
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <BarChart
            width={730}
            height={375}
            data={data}
            onClick={({ activePayload }) => {
              navigate(`/industries/${activePayload?.pop().payload.id}`);
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" hide />
            <YAxis width={67} tickFormatter={(value) => `$${value / 1000000}m`} />
            <Tooltip
              formatter={(value: any) =>
                new Intl.NumberFormat('en-US', {
                  style: 'currency',
                  currency: 'USD',
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0,
                  currencyDisplay: 'narrowSymbol',
                }).format(value)
              }
              labelStyle={{ color: '#000' }}
            />
            <Bar dataKey="Amount" fill="#8884d8" />
          </BarChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default IndustriesVisualization;
