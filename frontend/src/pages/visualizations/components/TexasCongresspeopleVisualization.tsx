import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { Pie, PieChart, Tooltip } from 'recharts';
import { getCongresspeople } from '../../../shared/APIRequests';

function addContributions(congresspeopleData: any, tempHouseData: any[], tempSenateData: any[]) {
  for (let congressperson = 0; congressperson < congresspeopleData.length; congressperson++) {
    let amount = 0;
    for (
      let industry = 0;
      industry < congresspeopleData[congressperson].major_contributing_industries.length;
      industry++
    ) {
      amount += congresspeopleData[congressperson].major_contributing_industries[industry].amount;
    }
    const congresspersonContribution = {
      name: congresspeopleData[congressperson].name,
      id: congresspeopleData[congressperson].cp_id,
      value: amount,
    };
    if (congresspeopleData[congressperson].chamber == 'senate') {
      tempSenateData.push(congresspersonContribution);
    } else {
      tempHouseData.push(congresspersonContribution);
    }
  }
}

const TexasCongresspeopleVisualization = () => {
  const [houseData, setHouseData] = useState<any[]>([]);
  const [senateData, setSenateData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  const navigate = useNavigate();

  useEffect(() => {
    const tempHouseData: any[] = [];
    const tempSenateData: any[] = [];
    Promise.all(
      Array.from(Array(2).keys()).map((i) => getCongresspeople(i + 1, '', '', 'TX', '', '', '', ''))
    ).then((responses: any[]) => {
      responses.forEach((response) =>
        addContributions(response['page'], tempHouseData, tempSenateData)
      );
      setHouseData(tempHouseData);
      setSenateData(tempSenateData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Texas Congresspeople Contributions
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <PieChart width={730} height={350}>
            <Tooltip
              formatter={(value: any) =>
                new Intl.NumberFormat('en-US', {
                  style: 'currency',
                  currency: 'USD',
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0,
                  currencyDisplay: 'narrowSymbol',
                }).format(value)
              }
            />
            <Pie
              data={houseData}
              dataKey="value"
              nameKey="name"
              cx="50%"
              cy="50%"
              outerRadius={100}
              fill="#8884d8"
              onClick={({ payload }) => {
                navigate(`/congresspeople/${payload.id}`);
              }}
            />
            <Pie
              data={senateData}
              dataKey="value"
              nameKey="name"
              cx="50%"
              cy="50%"
              innerRadius={120}
              outerRadius={160}
              fill="#82ca9d"
              onClick={({ payload }) => {
                navigate(`/congresspeople/${payload.id}`);
              }}
            />
          </PieChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default TexasCongresspeopleVisualization;
