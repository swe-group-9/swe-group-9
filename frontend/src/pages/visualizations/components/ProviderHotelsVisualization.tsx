import { CSSProperties, useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { getProviderHotels } from '../../../shared/APIRequests';

function addHotels(hotelData: any, tempData: any[]) {
  for (let hotel = 0; hotel < hotelData.length; hotel++) {
    const hotelRatingValue = {
      name: hotelData[hotel].name,
      starRating: hotelData[hotel].star_rating,
      guestRating: hotelData[hotel].guest_rating,
    };
    tempData.push(hotelRatingValue);
  }
}

// see https://github.com/recharts/recharts/issues/275 and https://github.com/recharts/recharts/blob/master/src/component/DefaultTooltipContent.tsx
const CustomTooltip = ({ active, payload }: any) => {
  if (!active || !payload || !payload.length) {
    return null;
  }

  const itemStyle = {
    display: 'block',
    paddingTop: 4,
    paddingBottom: 4,
    color: '#000',
  };

  const finalPayload = [{ value: payload[0].payload.name, separator: '' }, ...payload];

  const items = finalPayload.map((entry: any, i: number) => {
    return (
      <li className="recharts-tooltip-item" key={`tooltip-item-${i}`} style={itemStyle}>
        <span className="recharts-tooltip-item-name">{entry.name || ''}</span>
        <span className="recharts-tooltip-item-separator">
          {entry.separator === undefined ? ' : ' : entry.separator}
        </span>
        <span className="recharts-tooltip-item-value">{entry.value}</span>
      </li>
    );
  });

  const wrapperStyle: CSSProperties = {
    margin: 0,
    padding: 10,
    backgroundColor: '#fff',
    border: '1px solid #ccc',
    whiteSpace: 'nowrap',
  };

  return (
    <div className="recharts-default-tooltip" style={wrapperStyle}>
      <ul className="recharts-tooltip-item-list" style={{ padding: 0, margin: 0 }}>
        {items}
      </ul>
    </div>
  );
};

const ProviderHotelsVisualization = () => {
  const [data, setData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const tempData: any[] = [];
    getProviderHotels().then((response: any) => {
      addHotels(response, tempData);
      setData(tempData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Star and Guest Rating of Hotels
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <ScatterChart width={600} height={400} data={data}>
            <CartesianGrid />
            <XAxis type="number" dataKey="starRating" name="Star Rating" />
            <YAxis type="number" dataKey="guestRating" name="Guest Rating" />
            <Tooltip cursor={{ strokeDasharray: '3 3' }} content={<CustomTooltip />} />
            <Scatter name="A school" data={data} fill="#8884d8" />
          </ScatterChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default ProviderHotelsVisualization;
