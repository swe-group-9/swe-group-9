import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { Pie, PieChart, Tooltip } from 'recharts';
import { getProviderEvents } from '../../../shared/APIRequests';

function addEvents(eventsData: any, tempData: any[]) {
  let naturalAmount = 0;
  let culturalAmount = 0;
  let historicalAmount = 0;

  for (let event = 0; event < eventsData.length; event++) {
    if (eventsData[event].primary_kind == 'Natural') {
      naturalAmount++;
    } else if (eventsData[event].primary_kind == 'Cultural') {
      culturalAmount++;
    } else if (eventsData[event].primary_kind == 'Historic') {
      historicalAmount++;
    }
  }

  const naturalEvents = {
    name: 'Natural',
    amount: naturalAmount,
    fill: '#57c0e8',
  };

  const culturalEvents = {
    name: 'Cultural',
    amount: culturalAmount,
    fill: '#FF6565',
  };

  const historicalEvents = {
    name: 'Historical',
    amount: historicalAmount,
    fill: '#FFda83',
  };

  tempData.push(naturalEvents);
  tempData.push(culturalEvents);
  tempData.push(historicalEvents);
}

const ProviderEventsVisualization = () => {
  const [data, setData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const tempData: any[] = [];
    getProviderEvents().then((response: any) => {
      addEvents(response, tempData);
      setData(tempData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Distribution of Event Types
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <PieChart width={730} height={320}>
            <Tooltip />
            <Pie data={data} dataKey="amount" nameKey="name" cx="50%" cy="50%" outerRadius={150} />
          </PieChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default ProviderEventsVisualization;
