import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis } from 'recharts';
import { getIndustries } from '../../../shared/APIRequests';

function addSectorContributions(industriesData: any, tempData: Map<string, number>) {
  industriesData.forEach((industry: any) => {
    if (industry.total_contributed !== undefined && industry.total_contributed !== 0) {
      tempData.set(
        industry.sector,
        (tempData.get(industry.sector) || 0) + industry.total_contributed
      );
    }
  });
}

const SectorsVisualization = () => {
  const [data, setData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const tempData = new Map<string, number>();
    Promise.all(
      Array.from(Array(4).keys()).map((i) => getIndustries(i + 1, '', '', '', '', '', ''))
    ).then((responses: any[]) => {
      responses.forEach((response) => addSectorContributions(response['page'], tempData));
      const fullMark = Math.max(...Array.from(tempData.values()));
      const formattedTempData = Array.from(tempData, ([sector, amount]) => ({
        sector,
        amount,
        fullMark,
      }));
      setData(formattedTempData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Sector Contributions
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <RadarChart width={730} height={375} outerRadius="80%" data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="sector" tick={{ fill: '#666' }} />
            <PolarRadiusAxis angle={30} tickFormatter={(value) => `$${value / 1000000000}b`} />
            <Radar
              name="Amount Contributed"
              dataKey="amount"
              stroke="#8884d8"
              fill="#8884d8"
              fillOpacity={0.6}
            />
          </RadarChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default SectorsVisualization;
