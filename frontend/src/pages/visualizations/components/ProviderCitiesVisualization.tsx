import { useEffect, useState } from 'react';
import { Box, Typography, Grid, CircularProgress } from '@mui/material';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import { getProviderCities } from '../../../shared/APIRequests';

function addCities(citiesData: any, tempData: any[]) {
  for (let city = 0; city < citiesData.length; city++) {
    const cityRatingValue = {
      name: citiesData[city].name,
      avgRating: citiesData[city].avg_rating * 2,
      budgetValue: citiesData[city].budget_value,
      safetyValue: citiesData[city].safety_value,
    };
    tempData.push(cityRatingValue);
  }
}

const ProviderCitiesVisualization = () => {
  const [data, setData] = useState<any[]>([]);
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    const tempData: any[] = [];
    getProviderCities().then((response: any) => {
      addCities(response, tempData);
      setData(tempData);
      setLoaded(true);
    });
  }, []);

  return (
    <Box>
      <Typography gutterBottom variant="subtitle1" component="div" pt={2}>
        Average Rating and Budget Value of Cities
      </Typography>
      <Grid container justifyContent="center">
        {loaded ? (
          <AreaChart width={850} height={400} data={data}>
            <defs>
              <linearGradient id="colorAR" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#8884d8" stopOpacity={0} />
              </linearGradient>
              <linearGradient id="colorBV" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#FFda83" stopOpacity={0.8} />
                <stop offset="95%" stopColor="#FFda83" stopOpacity={0} />
              </linearGradient>
            </defs>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" hide />
            <YAxis domain={[0, 10]} />
            <Tooltip
              formatter={(value: number, name: string) => [
                value,
                name == 'avgRating' ? 'Average Rating' : 'Budget Value',
              ]}
              labelStyle={{ color: '#000' }}
            />
            <Area
              type="monotone"
              dataKey="avgRating"
              stroke="#8884d8"
              fillOpacity={1}
              fill="url(#colorAR)"
            />
            <Area
              type="monotone"
              dataKey="budgetValue"
              stroke="#FFda83"
              fillOpacity={1}
              fill="url(#colorBV)"
            />
          </AreaChart>
        ) : (
          <CircularProgress />
        )}
      </Grid>
    </Box>
  );
};

export default ProviderCitiesVisualization;
