from flask import request, Response
from models import (
    CommitteeSchema,
    CongresspersonSchema,
    IndustrySchema,
    app,
)
from queries import get_congresspeople_query, get_committee_query, get_industry_query

# Texas Votes https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/app.py
# used to follow the structure of the file


@app.route("/congresspeople", methods=["GET"])
def congresspeople():
    arguments = request.args.to_dict()

    if "page" in arguments and arguments["page"].isdigit():
        page = int(arguments["page"])
    else:
        page = 1

    per_page = 25

    congresspeople_query = get_congresspeople_query(arguments)

    query_results = congresspeople_query.paginate(page=page, per_page=per_page)
    results_as_dict = [dict(zip(row.keys(), row)) for row in query_results.items]
    page_items = CongresspersonSchema().dump(results_as_dict, many=True)

    return {"page": page_items, "count": query_results.total}


@app.route("/congresspeople/<cp_id>", methods=["GET"])
def congresspeople_id(cp_id):
    congressperson = get_congresspeople_query({"cp_id": cp_id}).first()

    if congressperson is None:
        return Response(status=404)
    results_as_dict = dict(zip(congressperson.keys(), congressperson))
    return CongresspersonSchema().dump(results_as_dict)


@app.route("/committees", methods=["GET"])
def committees():
    arguments = request.args.to_dict()

    if "page" in arguments and arguments["page"].isdigit():
        page = int(arguments["page"])
    else:
        page = 1

    per_page = 25
    committee_query = get_committee_query(arguments)

    query_results = committee_query.paginate(page=page, per_page=per_page)
    results_as_dict = [dict(zip(row.keys(), row)) for row in query_results.items]
    page_items = CommitteeSchema().dump(results_as_dict, many=True)

    return {"page": page_items, "count": query_results.total}


@app.route("/committees/<comm_id>", methods=["GET"])
def committees_id(comm_id):
    committee = get_committee_query({"comm_id": comm_id}).first()
    if committee is None:
        return Response(status=404)
    results_as_dict = dict(zip(committee.keys(), committee))
    return CommitteeSchema().dump(results_as_dict)


@app.route("/industries", methods=["GET"])
def industries():
    arguments = request.args.to_dict()

    if "page" in arguments and arguments["page"].isdigit():
        page = int(arguments["page"])
    else:
        page = 1

    per_page = 25
    industry_query = get_industry_query(arguments)

    query_results = industry_query.paginate(page=page, per_page=per_page)
    results_as_dict = [dict(zip(row.keys(), row)) for row in query_results.items]
    page_items = IndustrySchema().dump(results_as_dict, many=True)

    return {"page": page_items, "count": query_results.total}


@app.route("/industries/<ind_id>", methods=["GET"])
def industry_id(ind_id):
    industry = get_industry_query({"ind_id": ind_id}).first()
    if industry is None:
        return Response(status=404)
    results_as_dict = dict(zip(industry.keys(), industry))
    return IndustrySchema().dump(results_as_dict)


@app.route("/")
def hello_world():
    return (
        '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg"/>'
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
