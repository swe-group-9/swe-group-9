# Catching Congress - Backend

## Dependencies to Run Locally:

- docker
- make (optional)

## How To Do Docker Commands

To build an image with what is currently in your directory, run `make docker-build`. To create a container with this image that you can use to check commands, you can run `make docker-run` and you'll be in a Docker container that has all the dependencies you need. Note that this command mounts the current working directory on the `backend` directory of the Docker container so that edits made in the container propogate to the directory you are working on locally. If you would like to use the built image (without overwriting anything), just run `docker run --rm -it backend`.

### Dockerfile

Without make locally, you can run `sudo docker build -t flask-docker .` on Linux/Mac/WSL. For Windows, the command is `docker build -t flask-docker .`. This is run instead of `make docker-build`. Similarly, you can run `docker run --rm -it -v $PWD:/backend -w /backend -p 80:80 flask-docker` instead of `make docker-run`. If using the Windows command prompt, replace `$PWD` with `%cd%`.

## How To Use Code Quality Tools

### Checking Code

Once in the docker container (by running `make docker-run`), simply run `make check-format` to check formatting and `make pylint` to do code analysis. If you'd like to save time, you can make docker do this all in one command (ex. `docker run backend make check-format`).

Note that you will need to build the docker container if you have made any changes since it was last built. To avoid this, mount the current directory on the `backend` directory in Docker, by adding the arguments `-v $PWD:/backend -w /backend`. If using the Windows command prompt, replace `$PWD` with `%cd%`.

### Formatting Code

Similarly, you can format your files using Black by running `make format` in the Docker container or `docker run --rm -v $PWD:/backend -w /backend backend make format` in the local `backend` directory. If using the Windows command prompt, replace `$PWD` with `%cd%`.
