import os
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv


def init_database(app):
    load_dotenv()
    app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("DB_URI")
    return SQLAlchemy(app)
