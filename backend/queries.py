from sqlalchemy import func, union
from sqlalchemy.dialects.mysql import match
from sqlalchemy.sql import text
from models import (
    Committee,
    Congressperson,
    Industry,
    CommitteeMembership,
    CommitteeRelations,
    CongresspersonContributions,
    db,
)


def apply_filters(query, params, filter_map, contains=None):
    for filter_attr in filter_map:
        if filter_attr in params:
            if contains is not None and filter_attr in contains:
                query = query.filter(filter_map[filter_attr] == params[filter_attr])
            else:
                query = query.filter(
                    filter_map[filter_attr].contains(params[filter_attr])
                )
    return query


def apply_sort(query, params, order_map):
    if "sort" not in params:
        return query
    order_attr = order_map[params["sort"]]
    if "desc" in params:
        order_attr = order_attr.desc()
    return query.order_by(order_attr)


def apply_search(query, params, search_attrs):
    if "q" not in params:
        return query
    modified_q = " ".join(f"*{term.strip('*')}*" for term in params["q"].split())
    match_expr = match(*search_attrs, against=modified_q)
    return query.filter(match_expr.in_boolean_mode()).order_by(match_expr.desc())


def get_congresspeople_query(params):
    contributions = (
        db.session.query(
            CongresspersonContributions.cp_id,
            func.group_concat(
                CongresspersonContributions.amount.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_amount"),
            func.group_concat(
                Industry.ind_id.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_ind_id"),
            func.group_concat(
                Industry.name.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_name"),
        )
        .join(Industry)
        .group_by(CongresspersonContributions.cp_id)
        .cte()
    )

    committee_memberships = (
        db.session.query(
            CommitteeMembership.cp_id,
            func.group_concat(
                Committee.comm_id.op("ORDER BY", precedence=100)(
                    Committee.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("committees_comm_id"),
            func.group_concat(
                Committee.name.op("ORDER BY", precedence=100)(Committee.name.asc()).op(
                    "SEPARATOR"
                )(text("':'"))
            ).label("committees_name"),
            func.group_concat(
                CommitteeMembership.currently_a_member.op("ORDER BY", precedence=100)(
                    Committee.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("committees_currently_a_member"),
        )
        .join(Committee)
        .group_by(CommitteeMembership.cp_id)
        .cte()
    )

    query = (
        db.session.query(
            Congressperson.cp_id,
            Congressperson.crp_id,
            Congressperson.fec_candidate_id,
            Congressperson.google_entity_id,
            Congressperson.name,
            Congressperson.description,
            Congressperson.political_party,
            Congressperson.state,
            Congressperson.gender,
            Congressperson.date_of_birth,
            Congressperson.chamber,
            Congressperson.title,
            Congressperson.currently_in_office,
            Congressperson.percentage_votes_missed,
            Congressperson.percentage_votes_with_political_party,
            Congressperson.image_url,
            Congressperson.top_contributors,
            Congressperson.url,
            Congressperson.twitter_account,
            Congressperson.youtube_account,
            Congressperson.facebook_account,
            Congressperson.phone,
            Congressperson.office_address,
            contributions.c.major_contributing_industries_ind_id,
            contributions.c.major_contributing_industries_name,
            contributions.c.major_contributing_industries_amount,
            committee_memberships.c.committees_comm_id,
            committee_memberships.c.committees_name,
            committee_memberships.c.committees_currently_a_member,
        )
        .outerjoin(contributions, contributions.c.cp_id == Congressperson.cp_id)
        .outerjoin(
            committee_memberships, committee_memberships.c.cp_id == Congressperson.cp_id
        )
    )

    filter_map = {
        "political_party": Congressperson.political_party,
        "chamber": Congressperson.chamber,
        "state": Congressperson.state,
        "currently_in_office": Congressperson.currently_in_office,
        "cp_id": Congressperson.cp_id,
    }

    order_map = {
        **filter_map,
        "name": Congressperson.name,
    }

    search_columns = [
        Congressperson.name,
        Congressperson.state,
        Congressperson.description,
    ]

    filtered_query = apply_filters(query, params, filter_map)
    searched_query = apply_search(filtered_query, params, search_columns)
    return apply_sort(searched_query, params, order_map)


def get_committee_query(params):
    committee_contributions = (
        db.session.query(
            CongresspersonContributions.ind_id,
            Committee.comm_id,
            Industry.sector,
            func.sum(CongresspersonContributions.amount).label("amount"),
            Industry.name,
        )
        .join(Congressperson)
        .join(CommitteeMembership)
        .join(Committee)
        .join(Industry)
        .group_by(Industry.ind_id)
        .group_by(Committee.comm_id)
        .cte()
    )

    aggregated_committee_contributions = (
        db.session.query(
            committee_contributions.c.comm_id,
            func.group_concat(
                committee_contributions.c.ind_id.op("ORDER BY", precedence=100)(
                    committee_contributions.c.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_ind_id"),
            func.group_concat(
                committee_contributions.c.amount.op("ORDER BY", precedence=100)(
                    committee_contributions.c.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_amount"),
            func.group_concat(
                committee_contributions.c.name.op("ORDER BY", precedence=100)(
                    committee_contributions.c.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("major_contributing_industries_name"),
        )
        .group_by(committee_contributions.c.comm_id)
        .cte()
    )

    sectors_contributions = (
        db.session.query(
            committee_contributions.c.comm_id,
            committee_contributions.c.sector,
        )
        .group_by(
            committee_contributions.c.comm_id,
            committee_contributions.c.sector,
        )
        .cte()
    )

    aggregated_sectors = (
        db.session.query(
            sectors_contributions.c.comm_id,
            func.json_arrayagg(sectors_contributions.c.sector).label(
                "sectors_recieved_money_from"
            ),
        )
        .group_by(sectors_contributions.c.comm_id)
        .cte()
    )

    committee_members = (
        db.session.query(
            CommitteeMembership.comm_id,
            func.group_concat(
                Congressperson.cp_id.op("ORDER BY", precedence=100)(
                    Congressperson.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("members_cp_id"),
            func.group_concat(
                Congressperson.name.op("ORDER BY", precedence=100)(
                    Congressperson.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("members_name"),
            func.group_concat(
                Congressperson.state.op("ORDER BY", precedence=100)(
                    Congressperson.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("members_state"),
            func.group_concat(
                Congressperson.political_party.op("ORDER BY", precedence=100)(
                    Congressperson.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("members_political_party"),
            func.group_concat(
                CommitteeMembership.currently_a_member.op("ORDER BY", precedence=100)(
                    Congressperson.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("members_currently_a_member"),
        )
        .join(Congressperson)
        .join(Committee)
        .group_by(CommitteeMembership.comm_id)
        .cte()
    )

    chair = (
        db.session.query(
            Committee.comm_id,
            Committee.chair_id.label("chair_cp_id"),
            Congressperson.name.label("chair_name"),
            Congressperson.state.label("chair_state"),
            Congressperson.political_party.label("chair_political_party"),
            CommitteeMembership.currently_a_member.label("chair_currently_a_member"),
        )
        .join(
            CommitteeMembership,
            (CommitteeMembership.comm_id == Committee.comm_id)
            & (CommitteeMembership.cp_id == Committee.chair_id),
        )
        .join(Congressperson, Congressperson.cp_id == Committee.chair_id)
        .cte()
    )

    ranking_member = (
        db.session.query(
            Committee.comm_id,
            Committee.ranking_member_id.label("ranking_member_cp_id"),
            Congressperson.name.label("ranking_member_name"),
            Congressperson.state.label("ranking_member_state"),
            Congressperson.political_party.label("ranking_member_political_party"),
            CommitteeMembership.currently_a_member.label(
                "ranking_member_currently_a_member"
            ),
        )
        .join(
            CommitteeMembership,
            (CommitteeMembership.comm_id == Committee.comm_id)
            & (CommitteeMembership.cp_id == Committee.ranking_member_id),
        )
        .join(Congressperson, Congressperson.cp_id == Committee.ranking_member_id)
        .cte()
    )

    subcommittees = db.session.query(
        CommitteeRelations.subcommittee_id.label("associated_committee_comm_id"),
        CommitteeRelations.parent_committee_id.label("comm_id"),
    )

    parent_committees = db.session.query(
        CommitteeRelations.parent_committee_id.label("associated_committee_comm_id"),
        CommitteeRelations.subcommittee_id.label("comm_id"),
    )

    all_committee_associations = union(subcommittees, parent_committees).cte()

    associated_committees = (
        db.session.query(
            all_committee_associations.c.associated_committee_comm_id,
            all_committee_associations.c.comm_id,
            Committee.name,
            Committee.is_primary,
        )
        .join(
            Committee,
            Committee.comm_id
            == all_committee_associations.c.associated_committee_comm_id,
        )
        .cte()
    )

    aggregated_associated_committees = (
        db.session.query(
            associated_committees.c.comm_id,
            func.group_concat(
                associated_committees.c.associated_committee_comm_id.op(
                    "ORDER BY", precedence=100
                )(associated_committees.c.name.asc()).op("SEPARATOR")(text("':'"))
            ).label("associated_committees_comm_id"),
            func.group_concat(
                associated_committees.c.name.op("ORDER BY", precedence=100)(
                    associated_committees.c.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("associated_committees_name"),
            func.group_concat(
                associated_committees.c.is_primary.op("ORDER BY", precedence=100)(
                    associated_committees.c.name.asc()
                ).op("SEPARATOR")(text("':'"))
            ).label("associated_committees_is_primary"),
        )
        .group_by(associated_committees.c.comm_id)
        .cte()
    )

    num_subcommittees = (
        db.session.query(
            Committee.comm_id,
            func.count(CommitteeRelations.subcommittee_id).label("num_subcommittees"),
        )
        .outerjoin(
            CommitteeRelations,
            Committee.comm_id == CommitteeRelations.parent_committee_id,
        )
        .group_by(Committee.comm_id)
        .cte()
    )

    query = (
        db.session.query(
            Committee.comm_id,
            Committee.name,
            Committee.is_primary,
            Committee.chamber,
            Committee.ranking_member_id,
            Committee.url,
            aggregated_committee_contributions.c.major_contributing_industries_ind_id,
            aggregated_committee_contributions.c.major_contributing_industries_amount,
            aggregated_committee_contributions.c.major_contributing_industries_name,
            aggregated_sectors.c.sectors_recieved_money_from,
            committee_members.c.members_cp_id,
            committee_members.c.members_name,
            committee_members.c.members_state,
            committee_members.c.members_political_party,
            committee_members.c.members_currently_a_member,
            chair.c.chair_cp_id,
            chair.c.chair_name,
            chair.c.chair_state,
            chair.c.chair_political_party,
            chair.c.chair_currently_a_member,
            ranking_member.c.ranking_member_cp_id,
            ranking_member.c.ranking_member_name,
            ranking_member.c.ranking_member_state,
            ranking_member.c.ranking_member_political_party,
            ranking_member.c.ranking_member_currently_a_member,
            aggregated_associated_committees.c.associated_committees_comm_id,
            aggregated_associated_committees.c.associated_committees_name,
            aggregated_associated_committees.c.associated_committees_is_primary,
            num_subcommittees.c.num_subcommittees,
        )
        .outerjoin(
            aggregated_committee_contributions,
            aggregated_committee_contributions.c.comm_id == Committee.comm_id,
        )
        .outerjoin(
            aggregated_sectors, aggregated_sectors.c.comm_id == Committee.comm_id
        )
        .outerjoin(committee_members, committee_members.c.comm_id == Committee.comm_id)
        .outerjoin(chair, chair.c.comm_id == Committee.comm_id)
        .outerjoin(ranking_member, ranking_member.c.comm_id == Committee.comm_id)
        .outerjoin(
            aggregated_associated_committees,
            aggregated_associated_committees.c.comm_id == Committee.comm_id,
        )
        .outerjoin(num_subcommittees, num_subcommittees.c.comm_id == Committee.comm_id)
    )

    filter_map = {
        "chamber": Committee.chamber,
        "is_primary": Committee.is_primary,
        "associated_committees": aggregated_associated_committees.c.associated_committees_comm_id,
        "comm_id": Committee.comm_id,
    }

    order_map = {
        **filter_map,
        "name": Committee.name,
        "num_subcommittees": num_subcommittees.c.num_subcommittees,
    }

    search_columns = [Committee.name]

    filtered_query = apply_filters(
        query, params, filter_map, contains={"associated_subcommittees"}
    )
    searched_query = apply_search(filtered_query, params, search_columns)
    return apply_sort(searched_query, params, order_map)


def get_industry_query(params):
    congressperson_contributions = (
        db.session.query(
            CongresspersonContributions.ind_id,
            func.group_concat(
                Congressperson.cp_id.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("top_candidates_contributed_cp_id"),
            func.group_concat(
                CongresspersonContributions.amount.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("top_candidates_contributed_amount"),
            func.group_concat(
                Congressperson.name.op("ORDER BY", precedence=100)(
                    CongresspersonContributions.amount.desc()
                ).op("SEPARATOR")(text("':'"))
            ).label("top_candidates_contributed_name"),
        )
        .join(Congressperson)
        .group_by(CongresspersonContributions.ind_id)
        .cte()
    )

    aggregated_committee_contributions = (
        db.session.query(
            func.sum(CongresspersonContributions.amount).label("amount"),
            Committee.comm_id,
            Committee.name,
            CongresspersonContributions.ind_id,
        )
        .join(Congressperson)
        .join(CommitteeMembership)
        .join(Committee)
        .group_by(Committee.comm_id)
        .group_by(CongresspersonContributions.ind_id)
        .cte()
    )

    committee_contributions = (
        db.session.query(
            aggregated_committee_contributions.c.ind_id,
            func.group_concat(
                aggregated_committee_contributions.c.comm_id.op(
                    "ORDER BY", precedence=100
                )(aggregated_committee_contributions.c.amount.desc()).op("SEPARATOR")(
                    text("':'")
                )
            ).label("top_committees_contributed_comm_id"),
            func.group_concat(
                aggregated_committee_contributions.c.amount.op(
                    "ORDER BY", precedence=100
                )(aggregated_committee_contributions.c.amount.desc()).op("SEPARATOR")(
                    text("':'")
                )
            ).label("top_committees_contributed_amount"),
            func.group_concat(
                aggregated_committee_contributions.c.name.op(
                    "ORDER BY", precedence=100
                )(aggregated_committee_contributions.c.amount.desc()).op("SEPARATOR")(
                    text("':'")
                )
            ).label("top_committees_contributed_name"),
        )
        .group_by(aggregated_committee_contributions.c.ind_id)
        .cte()
    )

    query = (
        db.session.query(
            Industry.ind_id,
            Industry.name,
            Industry.sector,
            Industry.categories,
            Industry.total_contributed,
            Industry.party_most_contributed,
            Industry.chamber_most_contributed,
            Industry.state_most_contributed,
            Industry.top_contributors,
            congressperson_contributions.c.top_candidates_contributed_cp_id,
            congressperson_contributions.c.top_candidates_contributed_amount,
            congressperson_contributions.c.top_candidates_contributed_name,
            committee_contributions.c.top_committees_contributed_comm_id,
            committee_contributions.c.top_committees_contributed_amount,
            committee_contributions.c.top_committees_contributed_name,
        )
        .outerjoin(congressperson_contributions)
        .outerjoin(committee_contributions)
    )

    filter_map = {
        "sector": Industry.sector,
        "party_most_contributed": Industry.party_most_contributed,
        "chamber_most_contributed": Industry.chamber_most_contributed,
        "ind_id": Industry.ind_id,
    }

    order_map = {
        **filter_map,
        "name": Industry.name,
        "total_contributed": Industry.total_contributed,
    }

    search_columns = [Industry.name, Industry.sector]

    filtered_query = apply_filters(query, params, filter_map)
    searched_query = apply_search(filtered_query, params, search_columns)
    return apply_sort(searched_query, params, order_map)
