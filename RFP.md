- Group Number: 11am Group 9

- Team Members: Carson Saldanha, Nathan Gates, Nisha Ramesh, Daksh Dua, An Vi Nguyen

- Project Name: CatchingCongress

- GitLab URL: https://gitlab.com/catchingcongress/catchingcongress

- Project Proposal: Our website allows to user to check the industries that lobbied their US Congress representatives' campaigns, as well as the committees that they are in and the bills that they are passing.

- URLs of three disparate data sources:
    - https://www.opensecrets.org/open-data/api
    - https://projects.propublica.org/api-docs/congress-api/
    - https://developers.google.com/knowledge-graph
    - https://newsapi.org
    - https://api.open.fec.gov/developers/

- Models:
    - Congresspeople
    - Committees
    - Industries

- Instances of each model:
    - Congresspeople: 549
    - Committees: 250
    - Industries: 103

- Attributes for each model:
    - Congresspeople
        - Sortable/Filterable
            - Political Party
            - State
            - Gender
            - Currently In Office
            - Date of Birth
            - Legislative House
            - Percentage of Votes Missed
            - Percentage of Votes with Political Party
        - Searchable 
            - Name
            - Title
            - Major Contributing Industries
            - Top Contributors
            - Committee Membership
            - Short Description

    - Committees
        - Sortable/Filterable
            - Chamber (House, Senate, Joint)
            - Chair Party
            - Committee Status (subcommittee or primary committee)
            - Number of Subcommittees
            - Chair State
            - Sectors Committee has Recieved Money From
        - Searchable 
            - Name
            - Chair
            - Ranking Member
            - Sub/Parent Committees
            - Major Contributing Industries

    - Industries
        - Sortable/Filterable
            - Industry Sector
            - Total Amount Contributed To Candidates
            - Party Most Contributed To
            - Chamber Most Contributed To
            - State with Representatives Most Contributed To
        - Searchable 
            - Name
            - Subindustries
            - Top Contributing Organizations
            - Top Candidates Contributed To
            - Top Committees Contributed To


- Model connections:
    - Congresspeople are members of committees and recieve campaign funding from industries.
    - Committees have congresspeople as members and collectively recieve campaign funding from industries.
    - Industries contribute to the campaigns of congresspeople and to committees collectively.

- Media types:
    - Congresspeople
        - Twitter Feed (feed)
        - Google Knowledge Graph Image (image)
        - Google Knowledge Graph Description (text)
        - Office Location (embedded map)
        - Social Media Links (links)
        - News Feed (feed)

    - Committees
        - Committee Website (link)
        - News Feed (feed)
        - Bill Feed (feed)

    - Industries
        - Google Image Top Results (image)
        - News Feed (feed)

- Three questions answered from doing this data synthesis:
    - Which industries have most funded my representative?
    - Which congressional committees have recieved money from a specific industry sector?
    - Which political party has recieved the most money from a specific industry?
