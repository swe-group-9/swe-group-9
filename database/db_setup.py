"""Catching Congress Database Setup Python Script

This module contains functions that help set up the database for the Catching Congress website.
It can also be used as a command-line utility, which is likely the easiest way to use it. The code
in this module has also only been tested in Python 3.10. It may work with other versions of Python,
but there are no guarantees, so it is recommended to create and use a virtual environment with
version 3.10 of Python to avoid any issues with version incompatibility.

The script uses environment variables to avoid exposing sensitive information in source control.

    ROOT_DATABASE_USER
        This is the username for a 'root' user of the database. This user must have the ability to
        create databases and users, as well as flush privileges. This variable is only used when
        creating the database; it is not used otherwise.

    ROOT_DATABASE_PASSWORD
        This is the password for the root user of the password. This variable is only used when
        creating the database; it is not used otherwise.

    DATABASE_HOST
        This is the address of the MYSQL database host, used to connect to it.

    DATABASE_USER
        This is the username of user that is setup for this script to use. If creating the
        database, this environment variable will be used as the username for the script user that
        is created. Otherwise, this environment variable should contain the username of the script
        user that has already been created with the appropriate permissions.

    DATABASE_PASSWORD
        This is the password that is to be used for script user.

    DATABASE_NAME
        This is the name of the database. If creating the database, this environment variable is
        used as the name of the database. Otherwise, this environment variable should contain the
        name of the database that has already been created.

    READONLY_DATABASE_USER
        This is the username of the readonly user that created for use by the web app. If creating
        the database, this environment variable will be used as the username for the readonly user
        that is created. Otherwise, this variable is not used.

    READONLY_DATABASE_PASSWORD
        This is the password that is to be used for the readonly user that is created. Otherwise,
        this variable is not used.

Examples:
    Creates a database with two users, a setup user for this script and a readonly user to be used
    by the web app. It also fills in the tables with data from the JSON files in the directory.
        $ python db_setup.py -ct

    Using an already created database, drop the existing tables and create and fill them in.
        $ python db_setup.py -dt

    Using an already created database, drop the existing tables and create them without filling
    them in again.
        $ python db_setup.py -dtn

    Using an already created database, drop the existing tables and create them and fill them in
    with a custom file location for the congresspeople JSON file.
        $ python db_setup.py -dt --congresspeople custom.json

For more information, run:
    $ python db_setup.py --help
"""

from os import getenv
import json
import argparse
import mysql.connector
from dotenv import load_dotenv

load_dotenv()


def get_root_db_conn():
    """
    Gets a connection to the MYSQL database as a 'root' user.
    """
    return mysql.connector.connect(
        host=getenv("DATABASE_HOST"),
        user=getenv("ROOT_DATABASE_USER"),
        password=getenv("ROOT_DATABASE_PASSWORD"),
    )


def get_db_conn():
    """
    Gets a connection to the MYSQL database as the script user.
    """
    return mysql.connector.connect(
        host=getenv("DATABASE_HOST"),
        user=getenv("DATABASE_USER"),
        password=getenv("DATABASE_PASSWORD"),
        database=getenv("DATABASE_NAME"),
    )


def create_database():
    """
    Uses the 'root' user information to create the database and also the script and readonly users.
    """
    with get_root_db_conn() as conn, conn.cursor() as cursor:
        cursor.execute(f'CREATE DATABASE {getenv("DATABASE_NAME")}')
        cursor.execute(
            f"""CREATE USER '{getenv("DATABASE_USER")}'
        IDENTIFIED BY '{getenv("DATABASE_PASSWORD")}\'"""
        )
        cursor.execute(
            f'GRANT CREATE,DROP,INSERT,REFERENCES,ALTER ON prod.* to \'{getenv("DATABASE_USER")}\''
        )
        cursor.execute(
            f"""CREATE USER '{getenv("READONLY_DATABASE_USER")}'
            IDENTIFIED BY '{getenv("READONLY_DATABASE_PASSWORD")}\'"""
        )
        cursor.execute(
            f'GRANT SELECT ON prod.* to \'{getenv("READONLY_DATABASE_USER")}\''
        )
        cursor.execute("FLUSH PRIVILEGES")
        conn.commit()


def create_tables(conn):
    """
    Creates the tables in the MYSQL database. The following tables are created: congresspeople,
    committees, industries, committee_membership, committee_relations, and
    congresspeople_contributions.
    """
    with conn.cursor() as cursor:
        # MODEL TABLES
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS congresspeople (
                cp_id CHAR(7) NOT NULL UNIQUE,
                crp_id CHAR(9) NOT NULL UNIQUE,
                fec_candidate_id CHAR(9) NOT NULL UNIQUE,
                google_entity_id VARCHAR(255) NOT NULL UNIQUE,
                twitter_account VARCHAR(255) NOT NULL UNIQUE,
                facebook_account VARCHAR(255) UNIQUE,
                youtube_account VARCHAR(255) UNIQUE,
                url VARCHAR(255) NOT NULL UNIQUE,
                office_address VARCHAR(255) NOT NULL,
                phone CHAR(12) NOT NULL,
                political_party ENUM('D', 'R', 'I') NOT NULL,
                state CHAR(2) NOT NULL,
                gender CHAR(1) NOT NULL,
                currently_in_office BOOL NOT NULL,
                date_of_birth DATE NOT NULL,
                chamber ENUM('house', 'senate') NOT NULL,
                percentage_votes_missed FLOAT,
                percentage_votes_with_political_party FLOAT,
                name VARCHAR(255) NOT NULL UNIQUE,
                title VARCHAR(255) NOT NULL,
                top_contributors JSON NOT NULL,
                description TEXT NOT NULL,
                image_url VARCHAR(255) NOT NULL,
                PRIMARY KEY (cp_id)
            )
        """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS committees (
                comm_id VARCHAR(6) NOT NULL UNIQUE,
                chamber ENUM('house', 'senate', 'joint') NOT NULL,
                is_primary BOOL NOT NULL,
                name VARCHAR(255) NOT NULL,
                chair_id CHAR(7),
                ranking_member_id CHAR(7),
                url VARCHAR(255) NOT NULL,
                PRIMARY KEY (comm_id),
                FOREIGN KEY (chair_id) REFERENCES congresspeople (cp_id),
                FOREIGN KEY (ranking_member_id) REFERENCES congresspeople (cp_id)
            )
        """
        )

        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS industries (
                ind_id CHAR(3) NOT NULL UNIQUE,
                name VARCHAR(255) NOT NULL UNIQUE,
                sector VARCHAR(255) NOT NULL,
                categories JSON NOT NULL,
                total_contributed INT,
                party_most_contributed ENUM('D', 'R', 'I'),
                chamber_most_contributed ENUM('house', 'senate'),
                state_most_contributed CHAR(2),
                top_contributors JSON,
                PRIMARY KEY (ind_id)
            )
        """
        )

        # JOIN TABLES: Model many-to-many relations between the previous tables

        # Handles congressperson -> committees
        # Handles committee -> members
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS committee_membership (
                cp_id CHAR(7) NOT NULL,
                comm_id VARCHAR(6) NOT NULL,
                currently_a_member BOOL NOT NULL,
                PRIMARY KEY(cp_id, comm_id),
                FOREIGN KEY (cp_id) REFERENCES congresspeople (cp_id),
                FOREIGN KEY (comm_id) REFERENCES committees (comm_id)
            )
        """
        )

        # Handles committee -> associated_committees, committee -> num_subcommittees
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS committee_relations (
                parent_committee_id VARCHAR(6) NOT NULL,
                subcommittee_id VARCHAR(6) NOT NULL UNIQUE,
                PRIMARY KEY(parent_committee_id, subcommittee_id),
                FOREIGN KEY (parent_committee_id) REFERENCES committees (comm_id),
                FOREIGN KEY (subcommittee_id) REFERENCES committees (comm_id)
            )
        """
        )

        # Handles congressperson -> major_contributing_industries
        # Handles committee -> sectors_recieved_money_from, major_contributing_industries
        # Handles industry -> top_candidates_contributed, top_committees_contributed
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS congressperson_contributions (
                cp_id CHAR(7) NOT NULL,
                ind_id CHAR(3) NOT NULL,
                amount INT NOT NULL,
                PRIMARY KEY(cp_id, ind_id),
                FOREIGN KEY (cp_id) REFERENCES congresspeople (cp_id),
                FOREIGN KEY (ind_id) REFERENCES industries (ind_id)
            )
        """
        )
        conn.commit()


def drop_tables(conn):
    """
    Drops all existing SQL tables if they exist.
    """
    with conn.cursor() as cursor:
        cursor.execute("DROP TABLE IF EXISTS committee_membership")
        cursor.execute("DROP TABLE IF EXISTS committee_relations")
        cursor.execute("DROP TABLE IF EXISTS congressperson_contributions")
        cursor.execute("DROP TABLE IF EXISTS industries")
        cursor.execute("DROP TABLE IF EXISTS committees")
        cursor.execute("DROP TABLE IF EXISTS congresspeople")
        conn.commit()


def fill_tables(args, conn):
    """
    Fills all created tables with data from the JSON files.
    """
    with conn.cursor() as cursor:
        with open(args.congresspeople, encoding="utf-8") as file:
            congresspeople = json.load(file)
        cursor.executemany(
            """
            INSERT INTO congresspeople (
                cp_id,
                crp_id,
                fec_candidate_id,
                google_entity_id,
                twitter_account,
                facebook_account,
                youtube_account,
                url,
                office_address,
                phone,
                political_party,
                state,
                gender,
                currently_in_office,
                date_of_birth,
                chamber,
                percentage_votes_missed,
                percentage_votes_with_political_party,
                name,
                title,
                top_contributors,
                description,
                image_url
            ) VALUES (
                %(cp_id)s,
                %(crp_id)s,
                %(fec_candidate_id)s,
                %(google_entity_id)s,
                %(twitter_account)s,
                %(facebook_account)s,
                %(youtube_account)s,
                %(url)s,
                %(office_address)s,
                %(phone)s,
                %(political_party)s,
                %(state)s,
                %(gender)s,
                %(currently_in_office)s,
                %(date_of_birth)s,
                %(chamber)s,
                %(percentage_votes_missed)s,
                %(percentage_votes_with_political_party)s,
                %(name)s,
                %(title)s,
                %(top_contributors)s,
                %(description)s,
                %(image_url)s
            )""",
            congresspeople,
        )

        with open(args.committees, encoding="utf-8") as file:
            committees = json.load(file)
        cursor.executemany(
            """
            INSERT INTO committees (
                comm_id,
                chamber,
                is_primary,
                name,
                chair_id,
                ranking_member_id,
                url
            ) VALUES (
                %(comm_id)s,
                %(chamber)s,
                %(is_primary)s,
                %(name)s,
                %(chair_id)s,
                %(ranking_member_id)s,
                %(url)s
            )""",
            committees,
        )

        with open(args.industries, encoding="utf-8") as file:
            industries = json.load(file)
        cursor.executemany(
            """
            INSERT INTO industries (
                ind_id,
                name,
                sector,
                categories,
                total_contributed,
                party_most_contributed,
                chamber_most_contributed,
                state_most_contributed,
                top_contributors
            ) VALUES (
                %(ind_id)s,
                %(name)s,
                %(sector)s,
                %(categories)s,
                %(total_contributed)s,
                %(party_most_contributed)s,
                %(chamber_most_contributed)s,
                %(state_most_contributed)s,
                %(top_contributors)s
            )""",
            industries,
        )

        with open(args.committee_membership, encoding="utf-8") as file:
            committee_membership = json.load(file)
        cursor.executemany(
            """
            INSERT INTO committee_membership (
                cp_id, comm_id, currently_a_member
            ) VALUES (
                %(cp_id)s, %(comm_id)s, %(currently_a_member)s
            )""",
            committee_membership,
        )

        with open(args.committee_relations, encoding="utf-8") as file:
            committee_relations = json.load(file)
        cursor.executemany(
            """
            INSERT INTO committee_relations (
                parent_committee_id, subcommittee_id
            ) VALUES (
                %(parent_committee_id)s, %(subcommittee_id)s
            )""",
            committee_relations,
        )

        with open(args.congressperson_contributions, encoding="utf-8") as file:
            congressperson_contributions = json.load(file)
        cursor.executemany(
            """
            INSERT INTO congressperson_contributions (
                cp_id, ind_id, amount
            ) VALUES (
                %(cp_id)s, %(ind_id)s, %(amount)s
            )""",
            congressperson_contributions,
        )
        conn.commit()


def create_fulltext_indexes(conn):
    """
    Creates FULLTEXT indexes for the appropriate SQL tables.
    """
    with conn.cursor() as cursor:
        cursor.execute(
            "ALTER TABLE congresspeople ADD FULLTEXT(name, state, description)"
        )
        cursor.execute("ALTER TABLE committees ADD FULLTEXT(name)")
        cursor.execute("ALTER TABLE industries ADD FULLTEXT(name, sector)")
        conn.commit()


def main():
    """
    Parses the arguments from the command line for JSON file locations and also to decide which
    operations should be run on the MYSQL database.
    """
    parser = argparse.ArgumentParser(
        description="Sets up data for the website Catching Congress"
    )
    parser.add_argument(
        "-c", "--create_database", action="store_true", help="Create the SQL database"
    )
    parser.add_argument(
        "-d",
        "--drop",
        action="store_true",
        help="Drop existing tables before creating them again",
    )
    parser.add_argument(
        "-t", "--create-tables", action="store_true", help="Create the tables"
    )
    parser.add_argument(
        "-n",
        "--no-fill",
        action="store_true",
        help="Do not fill the SQL tables using JSON files",
    )
    parser.add_argument(
        "-f",
        "--fulltext",
        action="store_true",
        help="Create FULLTEXT indexes for the appropriate SQL tables",
    )
    parser.add_argument(
        "--congresspeople",
        default="congresspeople.json",
        help="Location of the congresspeople JSON file",
    )
    parser.add_argument(
        "--committees",
        default="committees.json",
        help="Location of the committees JSON file",
    )
    parser.add_argument(
        "--industries",
        default="industries.json",
        help="Location of the industry JSON file",
    )
    parser.add_argument(
        "--committee-membership",
        default="committee_membership.json",
        help="Location of the committee membership JSON file",
    )
    parser.add_argument(
        "--committee-relations",
        default="committee_relations.json",
        help="Location of the committee relations JSON file",
    )
    parser.add_argument(
        "--congressperson-contributions",
        default="congressperson_contributions.json",
        help="Location of the congressperson contributions by industry JSON file",
    )

    args = parser.parse_args()

    if args.create_database:
        create_database()

    with get_db_conn() as conn:
        if args.drop:
            drop_tables(conn)
        if args.create_tables:
            create_tables(conn)
        if not args.no_fill:
            fill_tables(args, conn)
        if args.fulltext:
            create_fulltext_indexes(conn)


if __name__ == "__main__":
    main()
