stages:
  - build
  - test
  - deploy

build-frontend:
  stage: build
  image: node:16
  needs: []
  cache:
    key: frontend
    paths:
      - frontend/node_modules/
  script:
    - cd frontend
    - echo "Building frontend React app"
    - npm install
    - CI=false npm run-script build

build-backend:
  stage: build
  image: python:3.9-slim-bullseye
  needs: []
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: backend
    paths:
      - .cache/pip
      - venv/
  script:
    - python -V
    - apt-get update
    - apt-get install -y build-essential make default-libmysqlclient-dev
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install -r backend/requirements.txt

frontend-format-tests:
  stage: test
  image: node:16
  needs: ["build-frontend"]
  cache:
    key: frontend
    paths:
      - frontend/node_modules/
  script:
    - cd frontend
    - echo "Checking Prettier format"
    - make check-format
    - echo "Checking ESLint analysis"
    - make eslint

frontend-unit-tests:
  stage: test
  image: node:16
  needs: ["build-frontend"]
  cache:
    key: frontend
    paths:
      - frontend/node_modules/
  script:
    - cd frontend
    - echo "Running Jest unit tests"
    - make unit-tests

frontend-gui-tests:
  stage: test
  image: nikolaik/python-nodejs:python3.9-nodejs16
  before_script:
    - pip install -r frontend/gui_tests/requirements.txt
    # Borrowed from https://gitlab.com/cs373-group-11/cultured-foodies/-/blob/master/.gitlab-ci.yml
    - apt-get update -q -y
    - apt-get --yes install libnss3
    - apt-get --yes install libgconf-2-4
    - apt-get install libx11-xcb1
    - curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | 
      apt-key add -
    - echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ 
      stable main" > /etc/apt/sources.list.d/google.list
    - apt-get update -q -y
    - apt-get install -y google-chrome-stable
    - version=$(curl -s "https://chromedriver.storage.googleapis.com/LATEST_RELEASE")
    - wget -qP /tmp/ "https://chromedriver.storage.googleapis.com/${version}/chromedriver_linux64.zip"
    - unzip -o /tmp/chromedriver_linux64.zip -d frontend/gui_tests/
  script:
    - cd frontend
    - echo "Running Selenium gui tests"
    - make gui-tests

database-format-tests:
  stage: test
  image: python:3.9-slim-bullseye
  needs: []
  before_script:
    - pip install -r database/requirements.txt
    - apt-get update
    - apt-get -y install make
  script:
    - cd database
    - echo "Checking Black format"
    - make check-format
    - echo "Checking Pylint analysis"
    - make pylint

backend-format-tests:
  stage: test
  image: python:3.9-slim-bullseye
  needs: ["build-backend"]
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: backend
    paths:
      - .cache/pip
      - venv/
  before_script:
    - apt-get update
    - apt-get install -y build-essential make default-libmysqlclient-dev
    - source venv/bin/activate
  script:
    - cd backend
    - echo "Checking Black format"
    - make check-format
    - echo "Checking Pylint analysis"
    - make pylint

backend-unit-tests:
  stage: test
  image: python:3.9-slim-bullseye
  needs: ["build-backend"]
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: backend
    paths:
      - .cache/pip
      - venv/
  before_script:
    - apt-get update
    - apt-get install -y build-essential make default-libmysqlclient-dev
    - source venv/bin/activate
  script:
    - cd backend
    - echo "Running Python unit tests"
    - make unit-tests

postman-tests:
  stage: test
  image:
    name: postman/newman:alpine
    entrypoint: [""]
  script:
    - cd backend
    - echo "Running Postman tests"
    - newman --version
    - newman run postman.json

deploy-backend:
  stage: deploy
  image: python:3.9-slim-bullseye
  needs: ["backend-format-tests", "backend-unit-tests"]
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  only:
    - main
  cache:
    key: backend
    paths:
      - .cache/pip
      - venv/
  before_script:
    - source venv/bin/activate
    - pip install awsebcli --upgrade
    - apt-get update
    - apt-get install -y build-essential git default-libmysqlclient-dev
    - git checkout main
  script:
    - cd backend
    - eb deploy cc-prod
